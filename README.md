# Templates Dojo
## Concept: AngularJS Online Store + Web API 2 RESTful back-end.

Templates Dojo is a fully functioning proof of concept web application built from the ground up. Templates Dojo
is a modular e-Commerce platform which poses as a website templates online store. It is built using
an AngularJS front-end coupled with Asp.Net WEB API 2 as a RESTful back-end. At the time of building Templates
Dojo, Web API 2 was still in a release candidate build which made this project a very challenging learning
experience. It also uses Twitter Bootstrap 3 for much of its styling.