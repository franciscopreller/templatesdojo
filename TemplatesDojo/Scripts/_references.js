/// <autosync enabled="true" />
/// <reference path="vendor/jquery-1.10.2.js" />
/// <reference path="vendor/angular/angular.js" />
/// <reference path="vendor/respond.js" />
/// <reference path="vendor/modernizr-2.6.2.js" />
/// <reference path="vendor/angular/angular-animate.js" />
/// <reference path="vendor/angular/angular-cookies.js" />
/// <reference path="vendor/angular/angular-loader.js" />
/// <reference path="vendor/angular/angular-mocks.js" />
/// <reference path="vendor/angular/angular-resource.js" />
/// <reference path="vendor/angular/angular-route.js" />
/// <reference path="vendor/angular/angular-sanitize.js" />
/// <reference path="vendor/angular/angular-scenario.js" />
/// <reference path="vendor/angular/angular-touch.js" />
/// <reference path="vendor/ui-bootstrap-0.6.0.js" />
/// <reference path="app/controllers/checkoutcontroller.js" />
/// <reference path="app/controllers/controlpanelcontroller.js" />
/// <reference path="app/controllers/edittemplatecontroller.js" />
/// <reference path="app/controllers/homecontroller.js" />
/// <reference path="app/controllers/managetemplatescontroller.js" />
/// <reference path="app/controllers/searchcontroller.js" />
/// <reference path="app/controllers/searchlistcontroller.js" />
/// <reference path="app/controllers/shoppingcartcontroller.js" />
/// <reference path="app/controllers/sitenavigationcontroller.js" />
/// <reference path="app/controllers/templateslistcontroller.js" />
/// <reference path="app/controllers/userauthenticationcontroller.js" />
/// <reference path="app/controllers/viewtemplatecontroller.js" />
/// <reference path="app/directives/addtocart.js" />
/// <reference path="app/directives/ajaxloading.js" />
/// <reference path="app/directives/breadcrumb.js" />
/// <reference path="app/directives/cartitems.js" />
/// <reference path="app/directives/collapse.js" />
/// <reference path="app/directives/dropdownhover.js" />
/// <reference path="app/directives/gravatar.js" />
/// <reference path="app/directives/passwordmatch.js" />
/// <reference path="app/directives/search.js" />
/// <reference path="app/directives/showbrand.js" />
/// <reference path="app/directives/sitenavigation.js" />
/// <reference path="app/directives/validatedform.js" />
/// <reference path="app/services/gravatarurlbuilder.js" />
/// <reference path="app/services/md5.js" />
/// <reference path="app/services/menudata.js" />
/// <reference path="app/services/pagesettings.js" />
/// <reference path="app/services/shoppingcartdata.js" />
/// <reference path="app/services/templatedata.js" />
/// <reference path="app/services/userdata.js" />
/// <reference path="app/app.js" />
/// <reference path="app/filters.js" />
/// <reference path="app/ie7mode.js" />
/// <reference path="app/routes.js" />
/// <reference path="vendor/html5shiv.js" />
/// <reference path="vendor/ieshiv.js" />
/// <reference path="vendor/json2.js" />
/// <reference path="vendor/restangular.js" />
/// <reference path="vendor/underscore-1.5.2.js" />
/// <reference path="app/controllers/alertscontroller.js" />
/// <reference path="app/services/alertdata.js" />
/// <reference path="app/controllers/managecategoriescontroller.js" />
/// <reference path="app/controllers/editcategorycontroller.js" />
/// <reference path="app/ajax-loading.js" />
/// <reference path="app/controllers/manageuserscontroller.js" />
/// <reference path="app/controllers/editusercontroller.js" />
/// <reference path="app/controllers/orderslistcontroller.js" />
/// <reference path="app/controllers/viewordercontroller.js" />
/// <reference path="app/directives/footerhelper.js" />
/// <reference path="app/services/themedata.js" />
/// <reference path="vendor/jquery.ui.widget.js" />
/// <reference path="vendor/canvas-to-blob.js" />
/// <reference path="vendor/jquery.blueimp-gallery.js" />
/// <reference path="vendor/jquery.fileupload-angular.js" />
/// <reference path="vendor/jquery.fileupload-image.js" />
/// <reference path="vendor/jquery.fileupload-process.js" />
/// <reference path="vendor/jquery.fileupload-validate.js" />
/// <reference path="vendor/jquery.fileupload.js" />
/// <reference path="vendor/jquery.iframe-transport.js" />
/// <reference path="vendor/blueimp-gallery.js" />
/// <reference path="app/controllers/imageuploadercontroller.js" />
/// <reference path="app/services/imagepreview.js" />
/// <reference path="vendor/load-image.min.js" />
/// <reference path="app/controllers/registerusercontroller.js" />
