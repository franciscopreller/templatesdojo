'use strict';

/* Filters */

app.filter('capitalize', function () {
    return function (input) {
        return input.charAt(0).toUpperCase() + input.slice(1);
    };
});

app.filter('orderNumber', function () {
    return function (input) {
        var number = input || 0;

        number += '';
        while (number.length < 5)
            number = '0' + number;

        return 'TD' + number;
    };
});