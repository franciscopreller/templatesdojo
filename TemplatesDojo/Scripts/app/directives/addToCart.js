'use strict';

app.directive('addToCart', [function() {
	return {
		restrict: 'AE',
		templateUrl: '/Content/ngViews/directives/add-to-cart.html',
		scope: {
		    item     : '=',
            licenseId: '@'
		},
		controller: ['$scope', '$location', 'shoppingCartData', 'Restangular', 'userData', 'alertData',

			function ($scope, $location, shoppingCartData, Restangular, userData, alertData) {

			    var licenseId = parseInt($scope.licenseId);

			    // Add license to scope
				$scope.license = {};
				Restangular.one('licenses', licenseId).get().then(function (license) {
				    $scope.license = license;
				});

				$scope.buttonType = licenseId === 1 ? 'btn-danger'  :
                                    licenseId === 2 ? 'btn-warning' : 'btn-success';

                // Adds text to the cart button
				$scope.buttonText = function() {
					var templates        = shoppingCartData.get(),
			    		shoppingCartItem = _(templates).findWhere({ templateId: $scope.item.templateID });

					if (!_.isEmpty(shoppingCartItem))
						return !(parseInt(shoppingCartItem.licenseId) !== licenseId) ? 'View Cart' :
							    (parseInt(shoppingCartItem.licenseId) < licenseId) ? 'Upgrade' :
                                'Downgrade';

					return 'Add to Cart';
				};

                // Adds icon to cart button
				$scope.buttonIcon = function() {
					var templates        = shoppingCartData.get(),
			    		shoppingCartItem = _(templates).findWhere({ templateId: $scope.item.templateID });

					return (_.isEmpty(shoppingCartItem)) ? 'shopping-cart' :
					       (parseInt(shoppingCartItem.licenseId) !== licenseId) ? 'transfer' :
                           'open';

				};


				$scope.addToCart = function () {

                    // Fail if admin attempts to add to cart
				    if (userData.isLoggedIn() && userData.get().access === 4) {
				        alertData.add('danger', 'Sorry, Site administrators may not purchase templates.');
				        return;
				    }

			    	// First find if another item with the same templateId is in the cart
			    	var templates = shoppingCartData.get(),
			    		template = _(templates).findWhere({ templateId: $scope.item.templateID });

			    	// If the item being clicked, is already in the cart, view the shopping cart
			    	if (!_.isEmpty(_(templates).findWhere({ templateId: $scope.item.templateID, licenseId: licenseId }))) {
			    		$location.path('/view-cart');
			    		return;
			    	}

			    	!_.isEmpty(template) ?
			    		shoppingCartData.change($scope.item.templateID, licenseId) :
                        shoppingCartData.add($scope.item.templateID, licenseId);
			    };

			    $scope.inCart = function() {
			    	var templates = shoppingCartData.get();
			    	return !_.isEmpty(_(templates).findWhere({ templateId: $scope.item.templateID, licenseId: licenseId }));
			    };

			}
		]
	};
}]);