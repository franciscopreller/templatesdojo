'use strict';

app.directive('passwordMatch', [function () {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ctrl) {
			var firstPassword = '#' + attrs.passwordMatch;

			element.add(firstPassword).on('keyup', function () {
				scope.$apply(function () {
					var val = element.val() === $(firstPassword).val();
					ctrl.$setValidity('passwordmatch', val);
				});
			});
		}
	};
}]);