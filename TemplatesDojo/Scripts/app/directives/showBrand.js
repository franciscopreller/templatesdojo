'use strict';

app.directive('showBrand', ['$compile', function($compile) {
    return {
        restrict: 'C',
        scope: {
            name: "@",
            icon: "@"
        },
        template:  '<img data-ng-src="{{ icon }}" alt="{{ name }}"> <span>{{ name }}</span>'
    };
}]);