'use strict';

app.directive('siteNavigation', function() {
	return {
		restrict: 'C',
		templateUrl: '/Content/ngViews/directives/site-navigation.html',
		controller: 'SiteNavigationController'
	};
});