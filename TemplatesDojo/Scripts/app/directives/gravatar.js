'use strict';

app.directive('gravatar', ['gravatarUrlBuilder', '$compile', function(gravatarUrlBuilder, $compile) {
	return {
		restrict: 'A',
		scope: {
			email: '='
		},
		template: '<img data-ng-src="{{ url }}" class="thumbnail">',
		link: function(scope, element, attrs) {

			scope.$watch(attrs.email, function(value) {

				if ((value !== null) && (value !== undefined) && ((value === '') || (null != value.match(/.*@.*\..{2}/))))
					scope.url  = gravatarUrlBuilder.get(value);

			});

			
		}
	};
}]);