'use strict';

app.directive('ajaxLoading', ['$rootScope', function($rootScope) {
	return {
		restrict: 'A',
		template: '<div class="ajax-loading-overlay"></div>',
		link: function(scope, element, attrs) {
			$rootScope.$watch('ajaxLoading', function(value) {
				if (value)
					$(element).fadeIn(100);
				else
					$(element).fadeOut(100);
			});
		}
	}
}]);