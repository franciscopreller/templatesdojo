'use strict';

app.directive('dropdownHover', ['$document', '$location', function ($document, $location) {
	var openElement = null,
		closeMenu   = angular.noop;

	return {
	    restrict: 'CA',
    	link: function(scope, element, attrs) {

    		closeMenu = function (event) {
	            if (event) {
	              	event.preventDefault();
	              	event.stopPropagation();
	            }
            	element.parent().removeClass('open');
            	closeMenu = angular.noop;
            	openElement = null;
          	};

	      	scope.$watch('$location.path', function() { closeMenu(); });

	      	element.bind('mouseenter', function (event) {

	        	var elementWasOpen = (element === openElement);

	        	event.preventDefault();
	        	event.stopPropagation();

		        if (!!openElement) {
		          	closeMenu();
		        }

		        if (!elementWasOpen) {
		          	element.parent().addClass('open');
		          	openElement = element;
		        }

	      	});

	      	element.parent().bind('mouseleave', closeMenu);
    	}
	};

}]);