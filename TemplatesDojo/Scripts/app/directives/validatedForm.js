'use strict';

app.directive('validatedForm', ['$sce', '$compile', function ($sce, $compile) {

    var domContent = null;

    return {
        restrict: 'AE',
		scope: true,
        transclude: true,
        templateUrl: '/Content/ngViews/directives/validated-form.html',
		controller: ['$scope', '$transclude', function($scope, $transclude) {
		    $transclude(function (clone, scope) {
		        domContent = angular.element('<div>').append(clone).html();
		        $scope.buttons = domContent;
		    });
		}],
		link: function (scope, element, attrs, model) {

            // Initialize form
		    scope.form = {};

			// Scope
		    scope.form.caption = attrs.caption;
		    scope.form.location = 'Content/ngViews/forms/' + attrs.name + '.html';
		    scope.form.showButtons = !(domContent.replace(' ', '') === '');

            // Get submit button
		    var submitButton = element.find('button[type="submit"]');

            // Watch form validity status and disable submit button
		    scope.$watch(attrs.name + '.$valid', function (formIsValid) {
		        formIsValid ? submitButton.removeAttr('disabled') : submitButton.attr('disabled', true);
		    });

		}
    };

}]);