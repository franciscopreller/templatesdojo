'use strict';

app.directive('cartItems', function() {
	return {
		restrict: 'AE',
		templateUrl: '/Content/ngViews/directives/cart-items.html',
		scope: {
			items: '=',
			editable: '@'
		},
		controller: 'ShoppingCartController'
	};
});