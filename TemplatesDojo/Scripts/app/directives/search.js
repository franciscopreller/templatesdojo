'use strict';

app.directive('search', ['$rootScope', '$location', '$timeout',

	function ($rootScope, $location, $timeout) {

		return {
			restrict: 'A',
			link: function(scope, element, attrs) {

			    element.submit(function () {

			        $rootScope.$apply(function () {

						$rootScope.ajaxLoading = true;

						$timeout(function() {

							$location.path('/search').search({s: scope.search});
							$('input[type="text"]', element).blur();
							scope.search = '';
							$rootScope.ajaxLoading = false;

						}, 1000);

					});
				});
			}
		};

	}]);