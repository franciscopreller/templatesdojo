﻿'use strict';

app.directive('footerHelper', function () {
    return {
        restrict: 'A',
        templateUrl: 'Content/ngViews/directives/footer-helper.html',
        controller: ['$scope', '$rootScope', 'alertData', '$timeout', 'themeData',

            function ($scope, $rootScope, alertData, $timeout, themeData) {

                $scope.themes = themeData.get();
                $scope.currentTheme = themeData.current();
                $scope.contactVisible = false;
                $scope.themesVisible = false;
                $scope.contactVisibilityPristine = true;
                $scope.themesVisibilityPristine = true;

                $scope.toggleContact = function () {
                    $scope.contactVisibilityPristine = false;
                    $scope.contactVisible = !$scope.contactVisible;
                };

                $scope.toggleThemeChanger = function () {
                    $scope.themesVisibilityPristine = false;
                    $scope.themesVisible = !$scope.themesVisible;
                };

                $scope.selectTheme = function (theme) {
                    themeData.set(theme.theme);
                };

                $scope.submit = function () {
                    $rootScope.ajaxLoading = true;

                    $timeout(function () {

                        $rootScope.ajaxLoading = false;
                        alertData.add('success', 'Your email would have been sent successfully, if this were not a demo!');
                        contactUs.reset();

                    }, 1000);
                };

                $scope.$watch(themeData.current, function (theme) {
                    $scope.currentTheme = theme;
                });

        }],
        link: function (scope, element, attrs) {

            // PS: Using jQuery animations here because I'm short on time, AngularJS has an awesome
            // advanced form of animations which I'm using for things like fade in and out
            // of the ajax overlay and view loading, but this could be done much more elegantly
            // with the use of CSS3 transitions + AngularJS ngAnimate

            scope.$watch('contactVisible', function () {

                var contactForm = element.find('.footer-contact-form');

                // Only toggle if the visibilityPristine attribute is false
                // This prevents the contact form toggling on initial load
                if (!scope.contactVisibilityPristine)
                    contactForm.slideToggle();

            });

            scope.$watch('themesVisible', function () {

                var themeChanger = element.find('.footer-theme-changer');

                if (!scope.themesVisibilityPristine)
                    themeChanger.slideToggle();

            });

        }
    };
});