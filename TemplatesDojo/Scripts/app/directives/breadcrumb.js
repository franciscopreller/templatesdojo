'use strict';

app.directive('breadcrumb', [function() {
	return {

		restrict: 'A',
		templateUrl: '/Content/ngViews/directives/breadcrumb.html',
		scope: {
			breadcrumbs: '=items'
		}

	};
}]);