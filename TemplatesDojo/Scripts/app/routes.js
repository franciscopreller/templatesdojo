'use strict';

app.config(['$routeProvider', function($routeProvider) {

	$routeProvider

		/*****************************
		 * Public
		 *****************************/

        .when('/', {
            title      : 'Highest Quality, Most Affordable Templates',
            templateUrl: '/Content/ngViews/home.html',
            controller : 'HomeController',
            access     : [1, 2, 3, 4]
        })

        .when('/page-not-found', {
            title      : 'Page Not Found',
            templateUrl: '/Content/ngViews/404.html',
            access     : [1, 2, 3, 4]
        })

        .when('/search', {
            title      : 'Search Results',
            templateUrl: '/Content/ngViews/templates-list.html',
            controller : 'SearchListController',
            access     : [1, 2, 3, 4]
        })

		.when('/how-it-works', {
            title      : 'How it Works',
			templateUrl: '/Content/ngViews/about.html',
            access     : [1, 2, 3, 4]
		})

		.when('/become-a-ninja', {
            title      : 'Make Money - Become a Template Dojo Ninja!',
			templateUrl: '/Content/ngViews/authors.html',
            access     : [1, 2, 3, 4]
		})

        .when('/categories/:category', {
            templateUrl: '/Content/ngViews/templates-list.html',
            controller: 'TemplatesListController',
            access     : [1, 2, 3, 4]
        })

        .when('/categories/:category/:subcategory', {
            templateUrl: '/Content/ngViews/templates-list.html',
            controller : 'TemplatesListController',
            access     : [1, 2, 3, 4]
        })

        .when('/item/:itemId', {
            templateUrl: '/Content/ngViews/view-template.html',
            controller : 'ViewTemplateController',
            resolve    : {
                template: ['Restangular', '$route', function (Restangular, $route) {
                    return Restangular.one('templates', $route.current.params.itemId).get()
                        .then(function (data) {
                            return data;
                        });
                }]
            },
            access     : [1, 2, 3, 4]
        })

        /*****************************
         * Public ONLY
         *****************************/

        .when('/sign-in', {
            templateUrl: '/Content/ngViews/sign-in.html',
            controller : 'UserAuthenticationController',
            access     : [1]
        })

        .when('/sign-up', {
            templateUrl: '/Content/ngViews/sign-up.html',
            controller : 'RegisterUserController',
            access     : [1]
        })

        /*****************************
		 * Members
		 *****************************/

		 .when('/control-panel', {
		 	templateUrl: '/Content/ngViews/control-panel.html',
		 	controller: 'ControlPanelController',
		 	resolve: {
		 	    user: ['Restangular', function (Restangular) {
		 	        return Restangular.one('accounts/user-info').get()
                        .then(function (data) {
                            return data;
                        });
		 	    }]
		 	},
		 	access     : [2, 3, 4]
		 })

         .when('/view/orders', {
            templateUrl: '/Content/ngViews/view-orders.html',
            controller: 'OrdersListController',
            access: [4, 3, 2]
         })

         .when('/view/order/:orderId', {
            templateUrl: '/Content/ngViews/view-order.html',
            controller: 'ViewOrderController',
            access: [4, 3, 2]
         })

         .when('/view-cart', {
            templateUrl: '/Content/ngViews/view-cart.html',
            controller : 'ShoppingCartController',
            access     : [2, 3]
         })

        .when('/checkout', {
            templateUrl: '/Content/ngViews/checkout.html',
            controller : 'CheckoutController',
            access     : [2, 3]
        })

        /*****************************
		 * Admin Only
		 *****************************/

        .when('/manage/categories', {
            templateUrl: '/Content/ngViews/admin.categories.html',
            controller: 'ManageCategoriesController',
            access: [4]
        })

        .when('/manage/categories/create', {
            templateUrl: '/Content/ngViews/admin.category.html',
            controller: 'EditCategoryController',
            access: [4]
        })

        .when('/manage/categories/edit/:itemId', {
            templateUrl: '/Content/ngViews/admin.category.html',
            controller: 'EditCategoryController',
            access: [4]
        })

        .when('/manage/templates', {
            templateUrl: '/Content/ngViews/admin.templates.html',
            controller: 'ManageTemplatesController',
            access: [3, 4]
        })

        .when('/manage/templates/create', {
            templateUrl: '/Content/ngViews/create.template.html',
            controller: 'EditTemplateController',
            access: [3, 4]
        })

        .when('/manage/templates/edit/:itemId', {
            templateUrl: '/Content/ngViews/edit.template.html',
            controller: 'EditTemplateController',
            access: [3, 4]
        })

        .when('/manage/templates/edit/:itemId/upload-images', {
            templateUrl: '/Content/ngViews/image-uploader.html',
            controller: 'ImageUploaderController',
            access: [3, 4]
        })

        .when('/manage/users', {
            templateUrl: '/Content/ngViews/admin.users.html',
            controller: 'ManageUsersController',
            access: [4]
        })

        .when('/manage/users/:userId', {
            templateUrl: '/Content/ngViews/admin.user.html',
            controller: 'EditUserController',
            access: [4]
        })

        /*****************************
         * Route Error Handle
         *****************************/

        .otherwise({
        	redirectTo: '/page-not-found'
        })

}]);