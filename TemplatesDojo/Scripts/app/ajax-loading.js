'use strict';

angular.module('ajaxLoading', [])

    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('ajaxInterceptor');
    }])

    .run(['$rootScope', function ($rootScope) {

        $rootScope.queueCount = 0;
        $rootScope.ajaxLoading = false;

        // If items are any more than 0, ajaxLoading should display loading
        $rootScope.$watch('queueCount', function (count) {

            // Ensure the value does not go below 0
            if ($rootScope.queueCount < 0)
                $rootScope.queueCount = 0;

            $rootScope.ajaxLoading = !($rootScope.queueCount === 0);
        });

    }])

    .factory('ajaxInterceptor', ['$rootScope', '$q', function ($rootScope, $q) {
        return {
            'request': function (config) {
                $rootScope.queueCount++;
                return config || $q.when(config);
            },

            'response': function (response) {
                // Ensure the value does not go below 0
                if ($rootScope.queueCount < 0)
                    $rootScope.queueCount = 0;

                $rootScope.queueCount--;

                return response || $q.when(response);
            },

            'responseError': function (rejection) {
                // Ensure the value does not go below 0
                if ($rootScope.queueCount < 0)
                    $rootScope.queueCount = 0;

                $rootScope.queueCount--;

                return $q.reject(rejection);
            }
        };
    }]);