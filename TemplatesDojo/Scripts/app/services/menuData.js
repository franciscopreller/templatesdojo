'use strict';

app.factory('menuData', ['Restangular', '$q', function (Restangular, $q) {

    var MenuData = {

        top: [
            { text: 'How it Works', href: '/how-it-works' },
			{ text: 'Become a Ninja', href: '/become-a-ninja' }
        ],
        user: [
            { divider: true },
		    { text: 'Control Panel', href: '/control-panel' },
			{ text: 'Purchase History', href: '/view/orders' },
        ],
        author: [
            { divider: true },
			{ text: 'Control Panel', href: '/control-panel' },
			{ text: 'Manage Templates', href: '/manage/templates' },
            { text: 'Purchase History', href: '/view/orders' }
        ],
        admin: [
            { text: 'Control Panel', href: '/control-panel' },
			{ text: 'Manage Users', href: '/manage/users' },
			{ text: 'Manage Categories', href: '/manage/categories' },
            { text: 'Manage Templates', href: '/manage/templates' },
			{ text: 'View Orders', href: '/view/orders' },
        ],

        bottom: [
            { text: 'Browse', href: '/categories/browse', children: [
            { text: 'Most Popular', href: '/popular' },
            { divider: true },
            { text: 'See the Newest', href: '/categories/browse?q=newest' },
            { text: 'Made by our Top Ninjas', href: '/categories/all?q=top-author' }]}
        ]

    };

	return {

        loaded: false,
	    get : function () { return this.loaded ? MenuData : null },
	    load: function () {

	        var dfd = $q.defer();

	        Restangular.all('categories/hierarchy/active').getList().then(function (categories) {

	            // Populate bottom menu
	            _(categories).each(function (category, i) {

	                var categoryCode    = category.parentCategory.code,
                        categorySection = {
                            text: category.parentCategory.name,
                            href: '/categories/' + categoryCode,
                            children: [
                                { text: 'Most Popular', href: '/popular/' + categoryCode },
                                { divider: true }
                            ]
                        };

                    // Add active subcategories
	                _(category.subcategories).each(function (subcategory) {
	                    categorySection.children.push({
	                        text: subcategory.name,
	                        href: '/categories/' + categoryCode + '/' + subcategory.code
	                    });
	                });

                    // Dump into MenuData.bottom
	                MenuData.bottom.push(categorySection);

	                dfd.resolve();

	            });

	        }, function () { dfd.reject(); });

            // Set loaded
	        this.loaded = true;

	        return dfd.promise;
	    }

	};

}]);
