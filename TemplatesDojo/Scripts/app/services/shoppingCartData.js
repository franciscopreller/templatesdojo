'use strict';

app.factory('shoppingCartData', ['$cookieStore', 'Restangular', '$q', '$rootScope',
	
	function ($cookieStore, Restangular, $q, $rootScope) {

		var ShoppingCartItems = [];
		var ShoppingCartItem = function(templateId, licenseId) {
			this.templateId = templateId;
			this.licenseId  = licenseId;
		};

		return {
			count: function() {
				return _(ShoppingCartItems).size();
			},
			get: function() {
				return ShoppingCartItems;
			},
			update: function(cart) {
				if ($cookieStore.get('shoppingCart') !== undefined) {
					ShoppingCartItems = $cookieStore.get('shoppingCart')
				}
			},
			change: function(templateId, licenseId) {
				this.remove(templateId);
				this.add(templateId, licenseId);
			},
			add: function (templateId, licenseId) {
			    var newItem  = new ShoppingCartItem(templateId, licenseId),
                    deferred = $q.defer();

                // Fetch from server
			    Restangular.one('templates', templateId).get().then(function (template) {
			        Restangular.one('licenses', licenseId).get().then(function (license) {

			            // Pricing
			            var pricing = {
			                1: template.pricing[0].price,
			                2: template.pricing[1].price,
			                3: template.pricing[2].price
			            };

                        // Set shopping cart item
			            newItem.templateName = template.name;
			            newItem.licenseName = license.name;
			            newItem.pricing = pricing;
			            ShoppingCartItems.push(newItem);

                        // Set cookie
			            $cookieStore.put('shoppingCart', ShoppingCartItems);

                        // Resolve promise
			            deferred.resolve();

			        });
			    });

			    return deferred.promise;
			},
			remove: function(templateId) {
				var items = _(ShoppingCartItems).filter(function(item) {
					return (item.templateId !== templateId);
				});
				ShoppingCartItems = items;
				$cookieStore.put('shoppingCart', ShoppingCartItems);
			},
			clear: function() {
				ShoppingCartItems = [];
				$cookieStore.remove('shoppingCart');
			},
			buy: function (userPayment) {

			    var orderModel = {
			        user: {
			            firstName: userPayment.user.firstName,
			            lastName : userPayment.user.lastName,
			            email    : userPayment.user.email,
			            telephone: userPayment.user.telephone,
                        address  : userPayment.user.address
			        },
                    payment    : {
                        useExisting: userPayment.payment.useExisting,
                        cardnumber : userPayment.payment.number.join().replace(/,/g, '').replace(/\*/g, ''),
                        expiry     : new Date(userPayment.payment.expiry.year, userPayment.payment.expiry.month),
                        nameOnCard : userPayment.payment.name
                    },
			        templates  : []
			    };

			    _(ShoppingCartItems).each(function (item) {
			        orderModel.templates.push({
			            templateID: item.templateId,
			            licenseID: item.licenseId,
                        price: item.pricing[item.licenseId]
			        });
			    });

			    return Restangular.all('orders').post(orderModel);
			}
		};

}]);