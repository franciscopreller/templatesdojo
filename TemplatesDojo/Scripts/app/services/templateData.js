'use strict';

app.factory('templateData', ['Restangular', function (Restangular) {

    var templates = Restangular.all('templates').getList();
    var licenses = Restangular.all('licenses');

    return {

        get: function (id) {
            return _.isUndefined(id) ? Restangular.all('templates') : Restangular.one('templates', id);
        },

        getByCategory: function (categoryId) {
            return Restangular.one('categories', categoryId).getList('templates');
        },

        getByKeywords: function (string) {
            var keywords = string.split(' ');

            return _(templates).filter(function (template) {
                var keywordArray = _.union(template.categories, template.tags.split(' '), template.colors.split(' '));

                return _(keywordArray).find(function (keyword) {
                    return _(keywords).contains(keyword);
                });
            });
        },

        licenseType: function (id) { return _(licenses).findWhere({ licenseID: id }) },

        licenseTypes: function () { return licenses }

    };

}]);