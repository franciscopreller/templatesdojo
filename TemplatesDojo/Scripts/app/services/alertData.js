﻿'use strict';

app.factory('alertData', [function () {

    var Alerts = [];

    return {

        get: function () { return Alerts; },
        add: function (type, message) { Alerts.push({ type: type, message: message }); },
        remove: function (index) { Alerts.splice(index, 1); }

    };

}]);