'use strict';

app.factory('gravatarUrlBuilder', ['md5', function(md5) {
	return {
		get: function(email) {
            var hash = md5.createHash(email.toLowerCase());
            return 'http://www.gravatar.com/avatar/' + hash + ".jpg?s=200&r=g&d=identicon";
        }
	};
}]);