﻿'use strict';

app.factory('themeData', ['$rootScope', '$cookieStore', function ($rootScope, $cookieStore) {

    var Themes = [
        { name: 'Dark Theme',  theme: 'dark' },
        { name: 'Light Theme', theme: 'light' },
        { name: 'Sand Theme',  theme: 'sand' },
        { name: 'Ocean Theme', theme: 'ocean' }
    ];

    return {

        get     : function () { return Themes; },
        current : function () { return $cookieStore.get('theme') || 'dark'; },

        set: function (theme) {
            $cookieStore.put('theme', theme);
            $rootScope.theme = theme;
        },
        init: function () {
            var self = this;
            self.set(self.current());
        }

    };

}]);