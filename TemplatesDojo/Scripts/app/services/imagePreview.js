﻿'use strict';

app.factory('imagePreview', ['$rootScope', function ($rootScope) {

    var ImagePreview = {
        active: false,
        src: null,
        name: null,
        caption: null
    };

    return {

        set: function (options) {

            if (options !== undefined) {
                ImagePreview = _.extend(ImagePreview, options);
                
                if (options.active) {
                    angular.element('.image-preview').show();
                }
            }

            $rootScope.imagePreviewing = ImagePreview;
        }

    };

}]);