'use strict';

app.factory('pageSettings', function() {

	var Page = {
		name:   'TemplatesDojo',
		suffix: ' Website Templates | Wordpress Themes | Easy Website | Magento | eCommerce | Templates Dojo'
	}

	return {

		title: function(message) {
			var title = Page.name + ': ';

			if (message !== undefined)
				title += message + ' -';

			return title + Page.suffix;
		},

		brand: function() {
			return Page.name;
		}

	};
});