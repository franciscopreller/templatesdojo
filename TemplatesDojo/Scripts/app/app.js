'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('storeApp',
    ['ui.bootstrap', 'ngCookies', 'restangular', 'md5', 'ngAnimate', 'ngRoute', 'ngSanitize', 'ajaxLoading', 'blueimp.fileupload'])

    .run(['$rootScope', 'userData', '$location', 'pageSettings', 'shoppingCartData', 'themeData', 'imagePreview', 'Restangular',

        function($rootScope, userData, $location, pageSettings, shoppingCartData, themeData, imagePreview, Restangular) {

            // Initialize theme
            themeData.init();

            // Initialize image preview
            imagePreview.set();

            // Update user state
            userData.updateUserState();

            // Get shopping cart state
            shoppingCartData.update();

            // Check user access
            $rootScope.$on('$routeChangeStart', function(event, next, current) {

                var access = next.access || [];

                if (!_(access).contains(userData.get().access))
                    userData.isLoggedIn() ?
                        $location.path('/page-not-found').replace() :
                        $location.path('/sign-in').replace();

                // Set title
                var message      = next.title || undefined;
                $rootScope.title = pageSettings.title(message);

            });
            
        }])

    .config(['RestangularProvider', '$locationProvider',

        function (RestangularProvider, $locationProvider) {

            // Set base path for Web API
            RestangularProvider.setBaseUrl('/api');

            // Set hash prefix for SEO benefits
            $locationProvider.hashPrefix('!');

        }]);