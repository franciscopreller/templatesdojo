﻿'use strict';

app.controller('AlertsController', ['$scope', 'alertData', function ($scope, alertData) {

    $scope.alerts = [];

    // Watch for changes in alert
    $scope.$watch(function () { return alertData.get(); }, function (alerts) {
        $scope.alerts = alerts;
    }, true);

    // Close an alert
    $scope.closeAlert = function (index) {
        alertData.remove(index);
    };

}]);