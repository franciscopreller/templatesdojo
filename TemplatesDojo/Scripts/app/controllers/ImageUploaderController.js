﻿'use strict';

app.controller('ImageUploaderController', ['$scope', '$http', '$filter', '$window', '$route', '$cookieStore', 'Restangular',

    function ($scope, $http, $filter, $window, $route, $cookieStore, Restangular) {

        var url = 'api/templates/' + $route.current.params.itemId + '/images';

        $scope.options = {
            url: url,
            type: 'POST'
        };

        $scope.options.headers = {
            'Authorization': $cookieStore.get('user').token,
            'VFS-Upload-Path': null
        };

        $scope.loadingFiles = true;

        var baseTemplate = Restangular.one('templates', $route.current.params.itemId);
        baseTemplate.all('images').getList().then(function (response) {
            $scope.loadingFiles = false;
            $scope.queue = response.files || [];
        }, function () {
            $scope.loadingFiles = false;
        });

        $scope.cancel = function () {
            $window.history.back();
        };

    }]);

app.controller('FileDestroyController', ['$scope', '$http', '$cookieStore',

    function ($scope, $http, $cookieStore) {

        var file = $scope.file,
            state;

        if (file.url) {

            file.$state = function () {
                return state;
            };

            file.$destroy = function () {
                state = 'pending';

                return $http({
                    url: file.deleteUrl,
                    method: file.deleteType,
                    headers: { Authorization: $cookieStore.get('user').token }
                }).then(function () {
                    state = 'resolved';
                    $scope.clear(file);
                }, function () {
                    state = 'rejected';
                });
            };

        } else if (!file.$cancel && !file._index) {

            file.$cancel = function () {
                $scope.clear(file);
            };

        }

    }]);