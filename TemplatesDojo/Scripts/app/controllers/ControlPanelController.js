'use strict';

app.controller('ControlPanelController', ['$scope', 'userData', 'Restangular', 'alertData', 'user',

    function ($scope, userData, Restangular, alertData, user) {

        $scope.user = user;
	    $scope.username = userData.get().userName;
	    $scope.password = {};

	    // Watch for changes in the user.email model and change the gravatar email value
	    $scope.$watch('user.email', function(value) {
		    if (value !== undefined)
			    $scope.email = value;
	    });

        // Save user details
	    $scope.saveDetails = function () {

	        var userInfo = Restangular.one('accounts/user-info');
	    
	        // Add the $scope.template into the editTemplate object
	        angular.extend(userInfo, $scope.user);
	        Restangular.restangularizeElement(userInfo.parentResource, userInfo, userInfo.route);

	        userInfo.post().then(function () {
	            alertData.add('success', 'Updated your personal details successfully!');
	        }, function () {
	            alertData.add('danger', 'Unable to change your personal details.');
	        });
	    };

        // Change user password
	    $scope.changePassword = function () {

	        var passwordChange = Restangular.one('accounts/change-password');

	        // Add the $scope.template into the editTemplate object
	        angular.extend(passwordChange, {
	            oldPassword: $scope.password.old,
	            newPassword: $scope.password.new,
                confirmPassword: $scope.password.confirm
	        });
	        Restangular.restangularizeElement(passwordChange.parentResource, passwordChange, passwordChange.route);

	        passwordChange.post().then(function () {
	            alertData.add('success', 'Changed your password successfully!');
	        }, function () {
	            alertData.add('danger', 'Unable to change your password.');
	        });
	    };

}]);