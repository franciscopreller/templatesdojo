﻿'use strict';

app.controller('ManageCategoriesController', ['$scope', 'Restangular', '$rootScope', '$location',

    function ($scope, Restangular, $rootScope, $location) {

        $scope.categories = [];

        Restangular.all('categories/manage').getList().then(function (categories) {
            $scope.categories = categories;
        });

        $scope.edit = function (categoryId) {
            $location.path('manage/categories/edit/' + categoryId);
        };

}]);