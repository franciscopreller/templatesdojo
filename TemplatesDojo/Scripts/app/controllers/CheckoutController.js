'use strict';

app.controller('CheckoutController', ['$scope', 'shoppingCartData', 'Restangular', 'alertData',

	function ($scope, shoppingCartData, Restangular, alertData) {

		$scope.step          = 1;
		$scope.items 		 = shoppingCartData.get();
		$scope.count 		 = shoppingCartData.count();
		$scope.user          = {};

	    // Set months for expiry
		$scope.months = [
			{ id: 0, name: 'January' },
			{ id: 1, name: 'February' },
			{ id: 2, name: 'March' },
			{ id: 3, name: 'April' },
			{ id: 4, name: 'May' },
			{ id: 5, name: 'June' },
			{ id: 6, name: 'July' },
			{ id: 7, name: 'August' },
			{ id: 8, name: 'September' },
			{ id: 9, name: 'October' },
			{ id: 10, name: 'November' },
			{ id: 11, name: 'December' }
		];

	    // Set years for expiry
		$scope.years = [];

		var thisYear = new Date().getFullYear();
		for (var year = thisYear - 5; year < thisYear + 10; year++) {
		    $scope.years.push(year);
		}

	    // Payment details defaults
		$scope.card = {
		    number: [],
		    expiry: {
		        month: 0,
		        year: (thisYear + 2)
		    },
		    name: '',
            useExisting: false
		};

		Restangular.one('accounts', 'user-info').get().then(function (user) {
		    $scope.user = user;

		    if (user.payment)
		        $scope.card.useExisting = true;
		});

		$scope.$watch('card.useExisting', function (value) {

		    if (value) {

		        $scope.card.number[1] = '****';
		        $scope.card.number[2] = '****';
		        $scope.card.number[3] = '****';
		        $scope.card.number[4] = $scope.user.payment.creditCardTrail;

		        var date = new Date($scope.user.payment.expiryDate);
		        $scope.card.expiry = {
		            month: date.getMonth(),
		            year : date.getFullYear()
		        };
		        $scope.card.name = $scope.user.payment.nameOnCard;

		    } else {

		        $scope.card = {
		            number: [],
		            expiry: {
		                month: 0,
		                year: (thisYear + 2)
		            },
		            name: '',
                    useExisting: false
		        };

		    }

		});

		$scope.getTotal = function() {
			var total = 0;
			_($scope.items).each(function(item) {
				total += item.template.pricing[item.license];
			});
			return total;
		};

		$scope.stepTo = function(step) {
			$scope.step = step;
		};

		$scope.payment = function () {

		    // Ensure expiry date on card is valid
		    var date        = new Date(),
                compareDate = new Date(date.getFullYear(), date.getMonth()),
                expiryDate  = new Date($scope.card.expiry.year, $scope.card.expiry.month);

		    if (expiryDate < compareDate) {
		        alertData.add('danger', 'Unable to complete transaction. Your card is expired.');
		        return;
		    }

		    // Purchase items
		    var userPayment = {
		        user: $scope.user,
		        payment: $scope.card
		    };

		    shoppingCartData.buy(userPayment).then(function (response) {
		        shoppingCartData.clear();
		        alertData.add('success', 'The transaction was processed successfully!');
		        $scope.step = 4;
		        $scope.orderNumber = response.orderID;
		    }, function () {
		        alertData.add('danger', 'Unable to process the transaction, try again later.');
		    });

		};

}]);