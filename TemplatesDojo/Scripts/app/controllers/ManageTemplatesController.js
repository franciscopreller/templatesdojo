﻿'use strict';

app.controller('ManageTemplatesController', ['$scope', 'Restangular', '$location', '$rootScope',

    function ($scope, Restangular, $location, $rootScope) {

        $scope.templates = [];

        Restangular.all('templates/manage').getList().then(function (templates) {
            $scope.templates = templates;
        });

        $scope.edit = function (id) {
            $location.path('/manage/templates/edit/' + id);
        };

    }]);