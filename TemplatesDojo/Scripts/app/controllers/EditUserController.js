﻿'use strict';

app.controller('EditUserController', ['$scope', 'Restangular', '$route', '$window', 'alertData',

    function ($scope, Restangular, $route, $window, alertData) {

        $scope.user = {};
        $scope.password = {};
        $scope.email = null;

        var editUser = Restangular.one('accounts/user', $route.current.params.userId);
        
        editUser.get().then(function (user) {
            $scope.user = user;
            $scope.email = user.email;
        });

        $scope.cancel = function () {
            $window.history.back();
        };

        $scope.toggleActive = function () {

            // Don't allow admin users to be made inactive
            if ($scope.user.role === 'Admin') {
                alertData.add('danger', "Cannot change an Administrator's user status to inactive.");
                return;
            }

            $scope.user.active = !$scope.user.active;

            var activeToggleModel = {
                userID: $scope.user.userID,
                active: $scope.user.active
            };

            editUser.all('toggle-status').post(activeToggleModel).then(function () {
                // No success message as there is visual feedback by the activate button changing
            }, function () {
                // However, if we fail, notify the user and also change the visual feedback
                alertData.add('danger', 'Unable to toggle the status of this user.');
                $scope.user.active = !$scope.user.active;
            });

        };

        $scope.changePassword = function () {
            var passwordChange = editUser.all('change-password')

            // Add the $scope.template into the editTemplate object
            angular.extend(passwordChange, {
                newPassword: $scope.password.new,
                confirmPassword: $scope.password.confirm
            });
            Restangular.restangularizeElement(passwordChange.parentResource, passwordChange, passwordChange.route);

            passwordChange.post().then(function () {
                alertData.add('success', "Changed user's password successfully!");
            }, function () {
                alertData.add('danger', "Unable to change this user's password.");
            });
        };

    }]);