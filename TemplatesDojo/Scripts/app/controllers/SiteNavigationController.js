'use strict';

app.controller('SiteNavigationController', ['$scope', 'menuData', 'userData', 'pageSettings', 

	function ($scope, menuData, userData, pageSettings) {

		$scope.collapsed  = true;	
		$scope.topMenu    = [];
		$scope.bottomMenu = [];
		$scope.userMenu   = [];
		$scope.showUser   = userData.isLoggedIn();
		$scope.brandName = pageSettings.brand();

	    // Initialize menuData
		menuData.load().then(function () {
		    $scope.topMenu = menuData.get().top;
		    $scope.bottomMenu = menuData.get().bottom;
		});

		// Watch user status and change user management widget
		$scope.$watch(userData.isLoggedIn, function(value) {
			$scope.showUser = value;
		});

		// Watch the actual user object
		$scope.$watch(userData.get, function (user) {

		    if (menuData.loaded) {
		        $scope.userMenu = user.access === 4 ? menuData.get().admin  :
							      user.access === 3 ? menuData.get().author :
							      user.access === 2 ? menuData.get().user   : [];
		    }

		}, true);

	}
	
]);
