﻿'use strict';

app.controller('OrdersListController', ['$scope', 'Restangular', 'userData', '$location',

    function ($scope, Restangular, userData, $location) {

        $scope.orders  = [];
        $scope.isAdmin = (userData.get().access === 4);

        Restangular.all('orders').getList().then(function (orders) {
            $scope.orders = orders;
        });

        $scope.view = function (orderId) {
            $location.path('view/order/' + orderId);
        };

        $scope.getTotal = function (order) {
            var total  = 0,
                prices = _(order.templates).pluck('price');

            _(prices).each(function (price) {
                total += parseFloat(price);
            })

            return total.toFixed(2);
        };

        $scope.getGrandTotal = function () {
            var total  = 0;

            _($scope.orders).each(function (order) {
                _(order.templates).each(function (template) {
                    total += parseFloat(template.price);
                });
            });

            return total.toFixed(2);
        };

}]);