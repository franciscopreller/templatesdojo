﻿'use strict';

app.controller('ViewOrderController', ['$scope', 'Restangular', '$route', 'alertData', '$window',

    function ($scope, Restangular, $route, alertData, $window) {

        $scope.order = {};

        Restangular.one('orders', $route.current.params.orderId).get().then(function (order) {
            $scope.order = order;
        });

        $scope.download = function (templateId) {
            alertData.add('info', 'Cute, you really thought you coud actually download files on the demo?');
        };

        $scope.cancel = function () {
            $window.history.back();
        };

        $scope.getTotal = function () {
            var total = 0;

            _($scope.order.templates).each(function (template) {
                total += parseFloat(template.price);
            });

            return total.toFixed(2);
        };

}]);