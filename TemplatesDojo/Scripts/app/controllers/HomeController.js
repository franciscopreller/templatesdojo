'use strict';

app.controller('HomeController', ['$scope', 'Restangular', function ($scope, Restangular) {

    $scope.newTemplates = [];

    $scope.signupToNewsletter = function () {
        alertData.add('info', 'Thanks for clicking the button. I did not keep your email address. But you will not receive a free template anyway since this is just a demo, sorry :(');
    };

    Restangular.all('templates/newest').getList().then(function (templates) {
        $scope.newTemplates = templates;
    });


}]);