﻿'use strict';

app.controller('RegisterUserController', ['$scope', 'Restangular', 'alertData', 'userData', '$location',

    function ($scope, Restangular, alertData, userData, $location) {

        $scope.user  = {};
        $scope.login = {};

        $scope.register = function () {

            var registerModel = {
                userName        : $scope.login.username,
                password        : $scope.login.password,
                confirmPassword : $scope.login.confirmPassword,
                firstName       : $scope.user.firstName,
                lastName        : $scope.user.lastName,
                email           : $scope.user.email,
                telephone       : $scope.user.telephone,
                address         : $scope.user.address
            };

            Restangular.all('accounts/register').post(registerModel).then(function (response) {
                alertData.add('success', 'Registration successful, please wait while we log you in.');
                var promise = userData.signIn($scope.login.username, $scope.login.password);

                promise.then(function (data) {

                    // If login successful, redirect to the control panel
                    $location.path('/control-panel');

                    $scope.incorrectLogin = false;

                    // If login failed, display error message for user
                }, function () { $scope.incorrectLogin = true; });

            }, function (response) {
                alertData.add('danger', 'Unable to register at this time, this is likely due to your username being in use already.');
            });

        };

}]);