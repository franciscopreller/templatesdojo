﻿'use strict';

app.controller('EditTemplateController', ['$scope', 'Restangular', '$route', '$window', '$rootScope', 'alertData', '$location', 'imagePreview',

    function ($scope, Restangular, $route, $window, $rootScope, alertData, $location, imagePreview) {

        $scope.template      = {};
        $scope.categories    = [];
        $scope.subcategories = [];
        $scope.files         = [];
        $scope.pricing       = [
            { licenseID: 1, price: "0.00" },
            { licenseID: 2, price: "0.00" },
            { licenseID: 3, price: "0.00" }
        ];
        
        Restangular.all('subcategories').getList().then(function (categories) {
            $scope.subcategories = categories;
        });

        // If itemId was passed in, fetch and assign the template
        if ($route.current.params.itemId !== undefined) {
            var editTemplate = Restangular.one('templates', $route.current.params.itemId);

            editTemplate.get().then(function (template) {
                $scope.template   = template;
                $scope.categories = template.categories;
                
                // Pricing, fix to 2 decimals
                _(template.pricing).each(function (_templatePricing, index) {
                    $scope.pricing[index].price = parseFloat(_templatePricing.price).toFixed(2);
                });
            });

            editTemplate.all('images').getList().then(function (images) {
                $scope.files = images.files;
            });
        }

        $scope.cancel = function() {
            $location.path('/manage/templates');
        };

        /***************************************
         * Template Images
         ******************************************/

        $scope.uploadFiles = function () {
            $location.path('/manage/templates/edit/' + $route.current.params.itemId + '/upload-images');
        };

        $scope.previewImage = function (file) {
            imagePreview.set({
                active: true,
                src: file.url,
                name: file.name,
                caption: file.caption
            });
        };

        $scope.deleteImage = function (id, index) {
            editTemplate.one('images', id).remove().then(function (response) {
                $scope.files.splice(index, 1);
            });
        };

        $scope.makeDefaultImage = function (templateImageID) {

            // Create model wrapper
            var defaultModel = {
                templateID      : $route.current.params.itemId,
                templateImageID : templateImageID,
                'default'       : true
            };

            editTemplate.one('images', templateImageID).customPUT(defaultModel).then(function () {

                // Remove other default
                _($scope.files).filter(function (file, index) {
                    if (file.default)
                        $scope.files[index].default = false;
                });

                // Add this one
                _($scope.files).filter(function (file, index) {
                    if (file.templateImageID == templateImageID)
                        $scope.files[index].default = true;
                });

            }, function () {
                alertData.add('danger', 'Unable to update default image.');
            });
        };

        $scope.updateImages = function () {

            // Create model wrapper to send
            var captionsModel = {
                templateID : $route.current.params.itemId,
                captions   : []
            };

            // Insert captions to model
            _($scope.files).each(function (file) {
                captionsModel.captions.push({
                    templateImageID: file.templateImageID,
                    caption: file.caption || ''
                });
            });

            editTemplate.all('images').customPUT(captionsModel).then(function () {
                alertData.add('success', 'Updated images successfully!');
            }, function () {
                alertData.add('danger', 'Unable to update images.');
            });
        };

        /***************************************
         * Template Showcase
         ******************************************/

        $scope.updateTemplateShowcase = function () {
            
            var showcaseModel = {
                templateID: $route.current.params.itemId,
                htmlShowcase: $scope.template.htmlShowcase
            };

            editTemplate.all('showcase').customPUT(showcaseModel).then(function (response) {
                alertData.add('success', 'Updated template showcase successfully!');
            }, function () {
                alertData.add('danger', 'Unable to update template showcase.');
            });

        };

        /***************************************
         * Template Categories
         ******************************************/

        $scope.assignCategory = function (oldCategory, newCategory) {

            var replacement = {
                categoryID: newCategory.categoryID,
                templateID: $scope.template.templateID,
                category  : newCategory
            };

            // If the passed category is undefined, we'll add a new one
            if (oldCategory === undefined)
                $scope.categories.push(replacement);

            // Otherwise we'll replace an existing
            else {
                _($scope.categories).each(function (category, index) {
                    if (category.categoryID == oldCategory.categoryID)
                        $scope.categories[index] = replacement;
                });
            }

        };

        $scope.updateTemplateCategories = function () {

            // Build a list of all category IDs
            var templateCategories = Restangular.copy($scope.categories),
                categoryIds = _(templateCategories).pluck('categoryID'),
                templateId = $scope.template.templateID;

            editTemplate.all('categories').customPUT({
                templateID: templateId,
                categoryIDs: categoryIds
            }).then(function () {
                alertData.add('success', 'Update categories for template successfully!');
            }, function () {
                alertData.add('danger', 'Unable to update categories for template.')
            });

        };

        $scope.removeCategory = function (categoryIndex) {
            $scope.categories.splice(categoryIndex, 1);
        };

        $scope.categoryExists = function (id) {
            return !(_.isEmpty(_($scope.categories).findWhere({ categoryID: id })));
        };

        /***************************************
         * Template Active Status
         ******************************************/

        $scope.toggleActive = function () {

            $scope.template.active = !$scope.template.active;

            var activeToggleModel = {
                templateID: $scope.template.templateID,
                active: $scope.template.active
            };

            editTemplate.all('toggle-status').post(activeToggleModel).then(function () {
                // No success message as there is visual feedback by the activate button changing
            }, function () {
                // However, if we fail, notify the user and also change the visual feedback
                alertData.add('danger', 'Unable to toggle the status of this template.');
                $scope.template.active = !$scope.template.active;
            });

        };

        /***************************************
         * Template Details
         ******************************************/

        $scope.updateTemplateDetails = function () {

            // Add the $scope.template into the editTemplate object
            angular.extend(editTemplate, $scope.template);
            Restangular.restangularizeElement(editTemplate.parentResource, editTemplate, editTemplate.route);

            editTemplate.put().then(function () {
                alertData.add('success', 'Updated template details successfully!');
            }, function () {
                alertData.add('danger', 'Unable to update template details.');
            });
        };

        /***************************************
         * Template Pricing
         ******************************************/

        $scope.updateTemplatePricing = function () {
            
            // Initialize the pricing model
            var pricingModel = {
                templateID: $scope.template.templateID,
                pricingModels: []
            };

            // Add the license and price objects to the pricing model
            _($scope.pricing).each(function (pricing) {
                pricingModel.pricingModels.push({
                    licenseID: parseInt(pricing.licenseID),
                    price: parseFloat(pricing.price)
                });
            });

            editTemplate.all('pricing').customPUT(pricingModel).then(function () {
                alertData.add('success', 'Updated pricing for template successfully!');
            }, function () {
                alertData.add('danger', 'Unable to update pricing for template.');
            });

        };

        /***************************************
         * Create Template
         ******************************************/

        $scope.save = function () {

            var newTemplate = $scope.template;
            newTemplate.categoryIDs = _($scope.categories).pluck('categoryID'),
            newTemplate.pricing = [];

            // Pricing, fix to 2 decimals
            _($scope.pricing).each(function (pricing, index) {
                newTemplate.pricing.push({
                    price: parseFloat(pricing.price),
                    licenseID: parseInt(pricing.licenseID)
                });
            });

            // Restangularize the element
            var template = Restangular.all('templates');
            angular.extend(template, newTemplate);
            Restangular.restangularizeElement(template.parentResource, template, template.route);

            template.post().then(function (response) {
                $location.path('/manage/templates/edit/' + response.templateID);
                alertData.add('success', 'Successfully created new template!');
            }, function () {
                alertData.add('danger', 'Unable to create new template.');
            });

        };

}]);