﻿'use strict';

app.controller('ManageUsersController', ['$scope', 'Restangular', '$location',

    function ($scope, Restangular, $location) {

        $scope.users = [];

        Restangular.all('accounts/users').getList().then(function (users) {
            $scope.users = users;
        });

        $scope.edit = function (id) {
            $location.path('manage/users/' + id);
        };

}]);