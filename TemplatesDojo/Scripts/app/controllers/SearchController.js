'use strict';

app.controller('SearchController', ['$scope', '$location', function ($scope, $location) {

	$scope.submitSearch = function () {
		$location.path('/search').search({s: $scope.search});
	};

	$scope.search = '';

}]);