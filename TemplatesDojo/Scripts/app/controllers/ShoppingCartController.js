'use strict';

app.controller('ShoppingCartController', ['$scope', 'shoppingCartData', 'Restangular',
	
	function ($scope, shoppingCartData, Restangular) {

		$scope.items = shoppingCartData.get();
		$scope.count = shoppingCartData.count();
		$scope.licenses = [];

		Restangular.all('licenses').getList().then(function (licenses) {
		    $scope.licenses = licenses;
		});

		$scope.$watch(shoppingCartData.count, function(value) {
			$scope.count = value;
		});

		$scope.$watch(shoppingCartData.get, function(value) {
			$scope.items = value
		});

		$scope.clear = function() {
			shoppingCartData.clear();
		};

		$scope.getTotal = function() {
			var total = 0;
			_($scope.items).each(function(item) {
			    total += item.pricing[item.licenseId];
			});
			return total;
		};

		$scope.remove = function(templateId) {
			shoppingCartData.remove(templateId);
		};

}]);