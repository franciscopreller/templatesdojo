'use strict';

app.controller('TemplatesListController', ['$scope', '$route', '$location', 'Restangular',
	function ($scope, $route, $location, Restangular) {

	    $scope.category  = {};
	    $scope.templates = [];

	    var categoryCode = $route.current.params.subcategory || $route.current.params.category;

        // Get categories
	    Restangular.one('categories', categoryCode).get().then(function (category) {

            // Set current scope's category
	        $scope.category = category;

	        var categoryId = $scope.category.categoryID;
		    // Get templates for current scope's category
		    Restangular.one('categories', categoryId).getList('templates').then(function (templates) {
		        $scope.templates = templates;
		    });

		});

	}]);