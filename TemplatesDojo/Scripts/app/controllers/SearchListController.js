'use strict';

app.controller('SearchListController', ['$scope', 'Restangular', '$location',

	function ($scope, Restangular, $location) {

		$scope.searchString = $location.search().s;
		$scope.category     = { name: 'Search Results' };
	    $scope.templates    = [];
	    $scope.searching    = true;

	    Restangular.all('templates/search?s=' + $scope.searchString).getList().then(function (templates) {
	        $scope.templates = templates;

	        var count = $scope.templates.length;
	        $scope.searchResults = count > 0 ? 'We found ' + count + ' item(s) you might find interesting, have a look!' :
                                               'Sorry... Your search returned nothing we could find.';
	    });

	}]);