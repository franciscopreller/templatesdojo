﻿'use strict';

app.controller('EditCategoryController', ['$scope', 'Restangular', '$rootScope', '$route', '$window', 'alertData', '$location',

    function ($scope, Restangular, $rootScope, $route, $window, alertData, $location) {

        $scope.edit = ($route.current.params.itemId !== undefined);
        $scope.category = {};
        $scope.parentCategories = [];

        Restangular.all('categories/main').getList().then(function (categories) {
            $scope.parentCategories = categories;
            $scope.category.parentCategoryID = categories[0].categoryID;
        });

        if ($scope.edit) {

            var editCategory = Restangular.one('categories', $route.current.params.itemId);
            editCategory.get().then(function (category) {
                $scope.category = category;
            });
        }

        $scope.cancel = function () {
            $location.path('/manage/categories');
        };

        $scope.toggleActive = function () {

            $scope.category.active = !$scope.category.active;

            var activeToggleModel = {
                categoryID: $scope.category.categoryID,
                active: $scope.category.active
            };

            editCategory.all('toggle-status').post(activeToggleModel).then(function () {
                // No success message as there is visual feedback by the activate button changing
            }, function (response) {
                // However, if we fail, notify the user and also change the visual feedback
                response.data.message ?
                    alertData.add('danger', response.data.message) :
                    alertData.add('danger', 'Unable to toggle the status of this category.');

                $scope.category.active = !$scope.category.active;
            });

        };

        $scope.save = function () {

            if ($scope.edit) {

                angular.extend(editCategory, $scope.category);
                Restangular.restangularizeElement(editCategory.parentResource, editCategory, editCategory.route);

                editCategory.put().then(function () {
                    alertData.add('success', 'Updated the category successfully!');
                }, function () {
                    alertData.add('danger', 'Unable to update the category.');
                });

            } else {

                var baseCategories = Restangular.all('categories'),
                    newCategory = Restangular.copy($scope.category);

                // Add active
                newCategory.Active = true;

                baseCategories.post(newCategory).then(function (response) {
                    alertData.add('success', 'Created a new category successfully!');
                    $location.path('/manage/categories/edit/' + response.categoryID);
                }, function () {
                    alertData.add('danger', 'Unable to create new category.');
                });
            }

        };

}]);