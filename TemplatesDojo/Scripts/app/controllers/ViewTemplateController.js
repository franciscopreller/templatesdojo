'use strict';

app.controller('ViewTemplateController', ['$scope', 'template', function ($scope, template) {

    $scope.template = template;

}]);