'use strict';

app.controller('UserAuthenticationController', ['$scope', '$location', 'userData', 'alertData', 'shoppingCartData',

    function ($scope, $location, userData, alertData, shoppingCartData) {

	    $scope.username = 'guest';
	    $scope.isAdmin = (userData.get().access === 4);

        // For warning message on login failed
	    $scope.incorrectLogin = false;

	    // Watch changed in User
	    $scope.$watch(userData.get, function(user) {
		    $scope.username = user.userName;
	    });

	    $scope.signIn = function() {

		    var promise = userData.signIn($scope.user.username, $scope.user.password);

		    promise.then(function (data) {

                // If login successful, redirect to the control panel
		        $location.path('/control-panel');

		        $scope.incorrectLogin = false;

            // If login failed, display error message for user
		    }, function () { $scope.incorrectLogin = true; });

	    };

	    $scope.signOut = function() {

		    userData.signOut().then(function(data) {
		        $location.path('/');
		        shoppingCartData.clear();
		        alertData.add('info', 'You have signed out. Thank you, come again!');
		    });

	    };

	    $scope.retrievePassword = function () {
	        alertData.add('info', 'This feature is disabled for the demo.');
	    };

}]);