'use strict';

app.config(['$sceProvider', function ($sceProvider) {

    // Completely disable SCE to support IE7.
    $sceProvider.enabled(false);

}]);