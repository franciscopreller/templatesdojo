﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace TemplatesDojo
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            // OWASP 9 & OWASP 6
            // -----------------
            // As the site is just a demo and due to time constraints I have not implemented transport layer protection.
            // However! If I WERE to include it, I would likely put some settings here to do it! Also, the web application
            // is deployed on IIS Express using Visual Studio, but if it were on a server, then there would be security
            // configurations to deal with. I am putting these two flags here to indicate that I've not forgotten about
            // these security risks, but they were somewhat inapplicable for this scenario.

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
