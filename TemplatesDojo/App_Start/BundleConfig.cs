﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace TemplatesDojo
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(
                "~/Scripts/vendor/jquery-1.10.2.js",
                "~/Scripts/vendor/underscore-1.5.2.js",
                "~/Scripts/vendor/jquery.ui.widget.js",
                "~/Scripts/vendor/load-image.min.js",
                "~/Scripts/vendor/canvas-to-blob.js",
                "~/Scripts/vendor/blueimp-gallery.js",
                "~/Scripts/vendor/jquery.blueimp-gallery.js",
                "~/Scripts/vendor/jquery.iframe-transport.js",
                "~/Scripts/vendor/jquery.fileupload.js",
                "~/Scripts/vendor/jquery.fileupload-process.js",
                "~/Scripts/vendor/jquery.fileupload-image.js",
                "~/Scripts/vendor/jquery.fileupload-validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/vendor/angular/angular.js",
                "~/Scripts/vendor/angular/angular-route.js",
                "~/Scripts/vendor/angular/angular-animate.js",
                "~/Scripts/vendor/angular/angular-cookies.js",
                "~/Scripts/vendor/angular/angular-sanitize.js",
                "~/Scripts/vendor/ui-bootstrap-0.6.0.js",
                "~/Scripts/vendor/restangular.js",
                "~/Scripts/ajax-loading.js",
                "~/Scripts/vendor/jquery.fileupload-angular.js"));

            bundles.Add(new ScriptBundle("~/bundles/app")
                .IncludeDirectory("~/Scripts/app", "*.js")
                .IncludeDirectory("~/Scripts/app/controllers", "*.js")
                .IncludeDirectory("~/Scripts/app/directives", "*.js")
                .IncludeDirectory("~/Scripts/app/services", "*.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/style.css"));

            // Support old browsers :(
            bundles.Add(new ScriptBundle("~/bundles/ltIE9").Include(
                "~/Scripts/vendor/html5shiv.js",
                "~/Scripts/vendor/respond.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/ltIE8").Include(
                "~/Scripts/vendor/ieshiv.js",
                "~/Scripts/vendor/json2.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/ie7mode").Include(
                "~/Scripts/ie7mode.js"
            ));

            bundles.Add(new StyleBundle("~/Content/ltIE8").Include(
                "~/Styles/bootstrap-ie7.css"
            ));

            // Optimizations :: True for production = Min & Concat output
            BundleTable.EnableOptimizations = false;

        }
    }
}
