﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TemplatesDojo.Models;
using TemplatesDojo.Logic;

namespace TemplatesDojo.Controllers
{
    [RoutePrefix("api/orders")]
    [Authorize]
    public class OrdersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private UserManager<ApplicationUser> UserManager { get; set; }

        public OrdersController()
        {
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        }

        // GET api/Orders
        [Route("")]
        public IQueryable<OrderModel> GetOrderModels()
        {
            var userId   = User.Identity.GetUserId();
            var userRole = UserManager.GetRoles(userId)[0];

            // If use is admin return all orders, Otherwise return only those which belong to the user
            return userRole == "Admin" ?
                db.OrderModels.Include(o => o.Templates).Include(o => o.User) :
                db.OrderModels.Include(o => o.Templates).Include(o => o.User)
                    .Where(o => o.UserID == userId);
        }

        // GET api/Orders/5
        [Route("{id:int}")]
        [ResponseType(typeof(OrderModel))]
        public async Task<IHttpActionResult> GetOrderModel(int id)
        {
            var userId = User.Identity.GetUserId();
            var userRole = UserManager.GetRoles(userId)[0];

            OrderModel ordermodel =
                await db.OrderModels
                    .Include(o => o.Templates)
                    .Include(o => o.User)
                    .FirstAsync(o => o.OrderID == id);

            if (ordermodel == null)
            {
                return NotFound();
            }

            if (userRole != "Admin" && ordermodel.UserID != userId)
            {
                return BadRequest();
            }

            return Ok(ordermodel);
        }

        // POST api/Orders
        [Route("", Name = "PostOrders")]
        [ResponseType(typeof(OrderBindingModel))]
        public async Task<IHttpActionResult> PostOrderModel(OrderBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Update user
            string userId = User.Identity.GetUserId();
            ApplicationUser user = UserManager.FindById(userId);
            user.FirstName = model.User.FirstName;
            user.LastName = model.User.LastName;
            user.Email = model.User.Email;
            user.Telephone = model.User.Telephone;
            user.Address = model.User.Address;

            IdentityResult result = await UserManager.UpdateAsync(user);
            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            int paymentId;
            // Does a new card need to be entered?
            if (model.Payment.UseExisting)
            {
                paymentId = db.PaymentModels.Single(p => p.UserID == userId && p.Active == true).PaymentID;
            }
            else
            {
                // Make any existing card inactive
                var activeCard = db.PaymentModels.FirstOrDefault(c => c.Active == true && c.UserID == userId);

                if (activeCard != null)
                {
                    activeCard.Active = false;

                    db.PaymentModels.Attach(activeCard);
                    db.Entry(activeCard).Property(c => c.Active).IsModified = true;
                    db.SaveChanges();
                }

                // OWASP 7
                // -------
                // The credit card information is stored here. I only did this due to a specific requirement on
                // the project to actually do this, otherwise I would have preferred to pass this job off to a
                // payment gateway. The credit card however, is hashed here using a SHA256 algorithm.

                var ccString = model.Payment.CardNumber;
                // Insert payment details to database
                PaymentModel paymentModel = new PaymentModel
                {
                    UserID = userId,
                    CreditCardHash  = Hash.Sha256(ccString),
                    CreditCardTrail = Convert.ToInt32(ccString.Substring(ccString.Length - 4)),
                    NameOnCard = model.Payment.NameOnCard,
                    ExpiryDate = model.Payment.Expiry,
                    Active = true
                };
                db.PaymentModels.Add(paymentModel);
                db.SaveChanges();

                paymentId = paymentModel.PaymentID;
            }

            // Create new order first
            OrderModel ordermodel = new OrderModel 
            { 
                OrderDate = DateTime.Now,
                UserID = User.Identity.GetUserId(),
                PaymentID = paymentId
            };

            db.OrderModels.Add(ordermodel);

            // Store
            await db.SaveChangesAsync();

            // Now create order templates
            foreach (OrderTemplateBindingModel template in model.Templates)
            {
                OrderTemplateModel templatemodel = new OrderTemplateModel
                {
                    OrderID = ordermodel.OrderID,
                    TemplateID = template.TemplateID,
                    LicenseID = template.LicenseID,
                    Price = template.Price
                };

                db.OrderTemplateModels.Add(templatemodel);

                await db.SaveChangesAsync();
            }

            return CreatedAtRoute("PostOrders", new { id = ordermodel.OrderID }, ordermodel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderModelExists(int id)
        {
            return db.OrderModels.Count(e => e.OrderID == id) > 0;
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

    }
}