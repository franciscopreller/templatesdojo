﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using TemplatesDojo.Models;
using TemplatesDojo.Providers;
using System.Web.Http.Description;

namespace TemplatesDojo.Controllers
{
    [Authorize]
    [RoutePrefix("api/accounts")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";

        public UserManager<ApplicationUser> UserManager { get; private set; }
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        public AccountController() : this(Startup.UserManagerFactory(), Startup.OAuthOptions.AccessTokenFormat)
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager, ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        // GET api/accounts/user-info
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("user-info")]
        public UserInfoViewModel GetUserInfo()
        {
            ApplicationUser user    = UserManager.FindById(User.Identity.GetUserId());
            IList<string> userRoles = UserManager.GetRoles(user.Id);
            int access = userRoles[0] == "Admin" ? 4 : userRoles[0] == "Author" ? 3 : userRoles[0] == "Member" ? 2 : 1;

            ApplicationDbContext db = new ApplicationDbContext();

            return new UserInfoViewModel
            {
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Address = user.Address,
                Telephone = user.Telephone,
                CreationDate = user.CreationDate,
                Access = access,
                Payment = db.PaymentModels.FirstOrDefault(p => p.UserID == user.Id && p.Active)
            };
        }

        // GET api/accounts/users
        [Authorize(Roles = "Admin")]
        [Route("users")]
        public UserViewModel[] GetUsers()
        {
            ApplicationDbContext userContext = new ApplicationDbContext();
            List<ApplicationUser> users = userContext.Users.ToList();
            List<UserViewModel> usersList = new List<UserViewModel>();

            foreach (ApplicationUser user in users)
            {
                usersList.Add(new UserViewModel
                {
                    UserID = user.Id,
                    UserName = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Address = user.Address,
                    Telephone = user.Telephone,
                    CreationDate = user.CreationDate,
                    Role = UserManager.GetRoles(user.Id)[0],
                    Active = user.Active
                });
            }

            return usersList.OrderBy(u => u.Role).ToArray();
        }

        // GET api/accounts/user/861c8844-87fa-4cd8-9b42-a00a05dbda0a
        [Authorize(Roles = "Admin")]
        [Route("user/{userId}")]
        public UserViewModel GetUser(string userId)
        {
            ApplicationUser user = UserManager.FindById(userId);
            IList<string> userRoles = UserManager.GetRoles(userId);

            return new UserViewModel
            {
                UserID = user.Id,
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Address = user.Address,
                Telephone = user.Telephone,
                CreationDate = user.CreationDate,
                Role = UserManager.GetRoles(user.Id)[0],
                Active = user.Active
            };
        }

        // POST api/accounts/user/861c8844-87fa-4cd8-9b42-a00a05dbda0a/change-password
        [Authorize(Roles = "Admin")]
        [Route("user/{userId}/change-password")]
        public async Task<IHttpActionResult> PostNewPassword(string userId, AdminChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Remove current password
            IdentityResult removePasswordResult = UserManager.RemovePassword(userId);
            IHttpActionResult removePasswordErrorResult = GetErrorResult(removePasswordResult);

            if (removePasswordErrorResult != null)
            {
                return removePasswordErrorResult;
            }

            IdentityResult result = await UserManager.AddPasswordAsync(userId, model.NewPassword);
            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        // POST api/accounts/user/861c8844-87fa-4cd8-9b42-a00a05dbda0a/toggle-status
        [Route("user/{userId}/toggle-status")]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> PostUserActiveModel(string userId, UserActiveBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userId != model.UserID)
            {
                return BadRequest();
            }

            // Get the category model
            ApplicationUser user = UserManager.FindById(userId);
            user.Active = model.Active;

            IdentityResult result = await UserManager.UpdateAsync(user);
            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        // POST api/accounts/user-info
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("user-info")]
        public async Task<IHttpActionResult> PostUserInfo(UserInfoBindingModel model)
        {
            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());

            // Update user
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;
            user.Telephone = model.Telephone;
            user.Address = model.Address;

            IdentityResult result = await UserManager.UpdateAsync(user);
            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        // POST api/accounts/logout
        [Route("logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // POST api/accounts/change-password
        [Route("change-password")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);
            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        // POST api/accounts/set-password
        [Route("set-password")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        // POST api/accounts/register
        [AllowAnonymous]
        [Route("register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApplicationUser user = new ApplicationUser
            {
                UserName = model.UserName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Address = model.Address,
                Telephone = model.Telephone,
                CreationDate = DateTime.Now,
                Active = true
            };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);
            IHttpActionResult errorResult = GetErrorResult(result);

            // Add new user to the member role
            UserManager.AddToRole(user.Id, "Member");

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserManager.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
