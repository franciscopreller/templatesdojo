﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TemplatesDojo.Models;

namespace TemplatesDojo.Controllers
{
    [RoutePrefix("api/categories")]
    public class CategoriesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/categories
        [Route("")]
        public IQueryable<CategoryModel> GetCategoryModels()
        {
            return db.CategoryModels;
        }

        [Route("main")]
        public IQueryable<CategoryModel> GetCategoryMainModels()
        {
            return db.CategoryModels.Where(c => c.ParentCategoryID == null);
        }

        [Route("manage")]
        [Authorize(Roles = "Admin")]
        public IQueryable<CategoryManagementViewModel> GetCategoryManagementModels()
        {
            return db.CategoryModels.Include(c => c.Category)
                .Where(c => c.ParentCategoryID.Value != null)
                .Select(c => new CategoryManagementViewModel
            {
                CategoryID = c.CategoryID,
                ParentCategoryID = c.ParentCategoryID.Value,
                ParentName = c.Category.Name,
                Name = c.Name,
                Code = c.Code,
                Active = c.Active
            });
        }

        // GET api/subcategories
        [Route("~/api/subcategories")]
        public IQueryable<CategoryModel> GetCategoryModelsSubcategories()
        {
            return db.CategoryModels
                .Where(c => c.ParentCategoryID != null)
                .OrderBy(c => c.ParentCategoryID)
                .ThenBy(c => c.Name);
        }

        // GET api/categories/hierarchy/active
        [Route("hierarchy/active")]
        public IQueryable<CategoryHierarchyBindingModel> GetCategoryModelsHierarchy()
        {
            List<CategoryHierarchyBindingModel> categoriesList = new List<CategoryHierarchyBindingModel>();
            var parentCategories = db.CategoryModels.Where(c => c.ParentCategoryID == null);

            // Loop over parent categories and insert children
            foreach (CategoryModel category in parentCategories)
            {
                // Get child categories to parent that are active
                var childCategories = db.CategoryModels
                    .Where(c => c.ParentCategoryID == category.CategoryID && c.Active);

                categoriesList.Add(new CategoryHierarchyBindingModel
                {
                    ParentCategory = category,
                    Subcategories  = childCategories.ToArray()
                });
            }

            return categoriesList.AsQueryable();
        }

        // GET api/categories/5
        [Route("{id:int}")]
        [ResponseType(typeof(CategoryModel))]
        public async Task<IHttpActionResult> GetCategoryModel(int id)
        {
            CategoryModel categorymodel = await db.CategoryModels.FindAsync(id);
            if (categorymodel == null)
            {
                return NotFound();
            }

            return Ok(categorymodel);
        }

        // GET api/categories/:categoryName
        [Route("{code}")]
        [ResponseType(typeof(CategoryModel))]
        public async Task<IHttpActionResult> GetCategoryModel(string code)
        {
            CategoryModel categorymodel = await db.CategoryModels.Where(c => c.Code == code).FirstOrDefaultAsync();
            if (categorymodel == null)
            {
                return NotFound();
            }

            return Ok(categorymodel);
        }

        // GET api/categories/5/templates
        [Route("{id:int}/templates")]
        [ResponseType(typeof(IQueryable<TemplateModel>))]
        public async Task<IHttpActionResult> GetCategoryTemplates(int id)
        {
            // Find out if we are on a parent category
            var category = await db.CategoryModels.FindAsync(id);
            bool isParent = (category.ParentCategoryID == null);

            // Get templates for chosen category
            IQueryable<TemplateModel> templates = null;
            if (!isParent)
            {
                templates = db.TemplateCategoryModels
                    .Where(tc => tc.CategoryID == id)
                    .Select(tc => tc.Template)
                    .Where(t => t.Active);
            }
            else
            {
                templates = db.TemplateCategoryModels
                    .Where(tc => tc.CategoryID == id || tc.Category.Category.CategoryID == id)
                    .Select(tc => tc.Template)
                    .Where(t => t.Active)
                    .Distinct();
            }

            if (templates == null)
            {
                return NotFound();
            }

            return Ok(templates);
        }

        [Route("{id:int}")]
        [Authorize(Roles = "Admin")]
        // PUT api/Categories/5
        public async Task<IHttpActionResult> PutCategoryModel(int id, CategoryModel categorymodel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categorymodel.CategoryID)
            {
                return BadRequest();
            }

            db.Entry(categorymodel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Categories
        [Route("", Name = "PostCategory")]
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(CategoryModel))]
        public async Task<IHttpActionResult> PostCategoryModel(CategoryModel categorymodel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            categorymodel.Active = true;

            db.CategoryModels.Add(categorymodel);
            await db.SaveChangesAsync();

            return CreatedAtRoute("PostCategory", new { id = categorymodel.CategoryID }, categorymodel);
        }

        // POST api/categories/5/toggle-status
        [Route("{id:int}/toggle-status", Name = "ToggleCategoryStatus")]
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(CategoryActiveBindingModel))]
        public async Task<IHttpActionResult> PostCategoryActiveModel(int id, CategoryActiveBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.CategoryID)
            {
                return BadRequest();
            }

            // Make sure that no templates are within this category first
            var templatemodel = await db.TemplateCategoryModels.FirstOrDefaultAsync(t => t.CategoryID == id);
            if (templatemodel != null)
            {
                return BadRequest("Cannot make a category inactive which still contains templates.");
            }

            // Get the category model
            var categorymodel = await db.CategoryModels.FindAsync(id);
            categorymodel.Active = model.Active;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("ToggleCategoryStatus", new { id = model.CategoryID }, model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryModelExists(int id)
        {
            return db.CategoryModels.Count(e => e.CategoryID == id) > 0;
        }
    }
}