﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TemplatesDojo.Models;
using System.Web;
using System.Security.Cryptography;
using System.IO;
using TemplatesDojo.Logic;

namespace TemplatesDojo.Controllers
{
    [RoutePrefix("api/templates")]
    public class TemplatesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public UserManager<ApplicationUser> UserManager { get; private set; }

        public TemplatesController()
        {
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        }

        /************************************************************
         * TemplateModel (General Public) Handling
         *************************************************************/

        // GET api/templates
        [Route("")]
        public IQueryable<TemplateModel> GetTemplateModels()
        {
            return db.TemplateModels;
        }

        // GET api/templates/5
        [Route("{id:int}")]
        [ResponseType(typeof(TemplateModel))]
        public async Task<IHttpActionResult> GetTemplateModel(int id)
        {
            TemplateModel templatemodel = await db.TemplateModels.FindAsync(id);
            if (templatemodel == null)
            {
                return NotFound();
            }

            return Ok(templatemodel);
        }

        [Route("newest")]
        public IQueryable<TemplateModel> GetNewestTemplateModels()
        {
            return db.TemplateModels.OrderByDescending(t => t.CreationDate).Take(24);
        }

        // GET api/templates/search?s=xyz
        [HttpGet]
        [Route("search")]
        public IQueryable<TemplateModel> GetTemplatesByKeywords(string s)
        {
            string[] keywords = s.Split(' ');

            return db.TemplateModels
                .Where(t => keywords.Any(word => t.Tags.Contains(word)) ||
                            keywords.Any(word => t.Name.Contains(word)) ||
                            keywords.Any(word => t.ShortDescription.Contains(word)))
                .Distinct();
        }

        /************************************************************
         * TemplateModel (Management) Handling
         *************************************************************/

        // OWASP 8
        // -------
        // Below, there is an [Authorize] annotation which is used throughout the entire Web API. This ensures that
        // the data being accessed via URL is correct for the user, EVEN if the user hijacks the front-end's
        // authentication layer by modifying the javascript code, they will need to validate against their
        // oAuth bearer token to get information out of the server.
        //
        // Please note: I only am pointing one [Authorize] annotation here, but they are used extensively throughout
        // the entire back-end server to tightly secure it.

        // Get api/templates/manage
        [Route("manage")]
        [Authorize(Roles = "Admin, Author")]
        public IQueryable<TemplateManagementViewModel> GetTemplateManageModels()
        {
            var userId = User.Identity.GetUserId();
            IList<string> userRoles = UserManager.GetRoles(userId);

            var model = db.TemplateModels
                .Include(t => t.Author).Select(t => new TemplateManagementViewModel
                {
                    TemplateID = t.TemplateID,
                    Name = t.Name,
                    CreationDate = t.CreationDate,
                    Downloads = t.Downloads,
                    Active = t.Active,
                    AuthorID = t.AuthorID,
                    AuthorName = t.Author.UserName
                });

            return (userRoles[0] == "Admin") ? model : model.Where(m => m.AuthorID == userId);
        }

        // PUT api/templates/5
        [Route("{id:int}")]
        [Authorize(Roles = "Admin, Author")]
        public async Task<IHttpActionResult> PutTemplateModel(int id, TemplateModel templatemodel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != templatemodel.TemplateID)
            {
                return BadRequest();
            }

            if (!TemplateModelBelongsToUserOrIsAdmin(id))
            {
                return BadRequest();
            }

            // Get current template and replicate the AuthorID
            TemplateModel model = await db.TemplateModels.FindAsync(id);
            templatemodel.AuthorID = model.AuthorID;

            db.Entry(model).CurrentValues.SetValues(templatemodel);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TemplateModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/templates
        [Route("", Name = "TemplatesPost")]
        [Authorize(Roles = "Admin, Author")]
        [ResponseType(typeof(TemplateBindingModel))]
        public async Task<IHttpActionResult> PostTemplateModel(TemplateBindingModel model)
        {
            // OWASP 1
            // -------
            // Entity Framework 6 is used over the entire application. No calls are made to the database
            // using SQL. This is a good layer of protection against SQL statements. As an added layer of
            // security, I also use view and binding models to ensure all the data is validated before
            // it reaches the database (see below: !ModelState.IsValid).
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Create the template model with the binding model details
            TemplateModel templatemodel = new TemplateModel();
            templatemodel.Name = model.Name;
            templatemodel.HTMLShowcase = model.HTMLShowcase;
            templatemodel.ShortDescription = model.ShortDescription;
            templatemodel.Tags = model.Tags;
            templatemodel.CreationDate = DateTime.Now;
            templatemodel.AuthorID = User.Identity.GetUserId();

            // Add the template model to the context
            db.TemplateModels.Add(templatemodel);

            // Save changes
            db.SaveChanges();

            // Get the templateId
            int templateId = templatemodel.TemplateID;

            // Now add the categories
            foreach (int categoryId in model.CategoryIDs)
            {
                db.TemplateCategoryModels.Add(new TemplateCategoryModel
                {
                    CategoryID = categoryId,
                    TemplateID = templateId
                });
            }

            // Finally, add the pricing
            foreach (TemplateLicensePriceBindingModel pricemodel in model.Pricing)
            {
                db.TemplatePricingModels.Add(new TemplatePricingModel
                {
                    TemplateID = templateId,
                    LicenseID = pricemodel.LicenseID,
                    Price = pricemodel.Price
                });
            }

            await db.SaveChangesAsync();

            // OWASP 10
            // --------
            // Asp.Net Web API 2 supports the CreatedAtRoute call as shown below. This authenticates and validates
            // the redirect route of the data back to the client-side. If the framework finds that the method above
            // was caught from a different route than the passed parameter, the call will fail.
            return CreatedAtRoute("TemplatesPost", new { id = templatemodel.TemplateID }, templatemodel);
        }

        // POST api/templates/5/toggle-status
        [Route("{id:int}/toggle-status", Name = "ToggleTemplateStatus")]
        [Authorize(Roles = "Admin, Author")]
        [ResponseType(typeof(TemplateActiveBindingModel))]
        public async Task<IHttpActionResult> PostCategoryActiveModel(int id, TemplateActiveBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.TemplateID)
            {
                return BadRequest();
            }

            // OWASP 4
            // -------
            // The back-end server implements calls such as this throughout its entirety. Even though I use authorize
            // annotations all throughout the application, this ensures that the resources being editted actually
            // belong to the user who's bearer token is accessing it. So even though I could manually send a POST
            // request to /api/templates/5/toggle-status even though resource #5 does not belong to me but my bearer
            // token does allow me access to it (for example: Author role). The request will fail here as the
            // resource does not belong to me.
            if (!TemplateModelBelongsToUserOrIsAdmin(id))
            {
                return BadRequest();
            }

            // Get the category model
            var templatemodel = await db.TemplateModels.FindAsync(id);
            templatemodel.Active = model.Active;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TemplateModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("ToggleTemplateStatus", new { id = model.TemplateID }, model);
        }

        /************************************************************
         * TemplatePricingyModel Handling
         *************************************************************/

        // PUT api/templates/5/pricing
        [Route("{id:int}/pricing")]
        [Authorize(Roles = "Admin, Author")]
        public async Task<IHttpActionResult> PutTemplatePricingModel(int id, TemplatePricingBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.TemplateID)
            {
                return BadRequest();
            }

            if (!TemplateModelBelongsToUserOrIsAdmin(id))
            {
                return BadRequest();
            }

            // Iterate through the binding model and edit license entries
            foreach (TemplateLicensePriceBindingModel priceModel in model.PricingModels)
            {
                TemplatePricingModel templatePricingModel = new TemplatePricingModel
                {
                    TemplateID = model.TemplateID,
                    LicenseID = priceModel.LicenseID,
                    Price = priceModel.Price
                };

                db.Entry(templatePricingModel).State = EntityState.Modified;
            }

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TemplateModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /************************************************************
         * TemplateCategoryModel Handling
         *************************************************************/

        // PUT api/templates/5/categories
        [Authorize(Roles = "Admin, Author")]
        [Route("{id:int}/categories")]
        [ResponseType(typeof(TemplateCategoriesBindingModel))]
        public async Task<IHttpActionResult> PutTemplateCategoriesModel(int id, TemplateCategoriesBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.TemplateID)
            {
                return BadRequest();
            }

            if (!TemplateModelBelongsToUserOrIsAdmin(id))
            {
                return BadRequest();
            }

            // Get existing models
            var existing = db.TemplateCategoryModels.Where(tc => tc.TemplateID == model.TemplateID);

            // Create a container to put the existing category IDs for late comparison
            List<int> existingIDs = new List<int>();

            // Iterate through the model and remove all items that are not in the list
            foreach (TemplateCategoryModel cat in existing)
            {
                if (!model.CategoryIDs.Contains(cat.CategoryID))
                    db.TemplateCategoryModels.Remove(cat);

                else
                    existingIDs.Add(cat.CategoryID);
            }

            // Now iterate threough the category IDs list and add the remaining
            foreach (int categoryID in model.CategoryIDs)
            {
                if (!existingIDs.ToArray().Contains(categoryID))
                {
                    db.TemplateCategoryModels.Add(new TemplateCategoryModel
                    {
                        TemplateID = model.TemplateID,
                        CategoryID = categoryID
                    });
                }
            }

            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        /************************************************************
         * TemplateImageModel Handling
         *************************************************************/

        // GET api/templates/5/images
        [Route("{id:int}/images")]
        [Authorize(Roles = "Admin, Author")]
        public TemplateImagesBindingModel GetTemplateImageModels(int id)
        {
            var templateimagemodels = db.TemplateImageModels.Where(t => t.TemplateID == id);
            List<TemplateImageUploadedBindingModel> files = new List<TemplateImageUploadedBindingModel>();

            // Note: Most of this could be refactored into the UploadHandler.cs file but
            //       due to time constraints, we'll do it quick and dirty.
            //
            // TODO: Refactor
            foreach (TemplateImageModel templateimagemodel in templateimagemodels)
            {
                // Get file info from the file system
                FileInfo file = new FileInfo(String.Format(
                    "{0}\\{1}",
                    HttpContext.Current.Server.MapPath("/Content/img/uploaded"),
                    templateimagemodel.ImagePath
                ));

                files.Add(new TemplateImageUploadedBindingModel
                {
                    TemplateImageID = templateimagemodel.TemplateImageID,
                    Name = templateimagemodel.Name,
                    Size = Convert.ToInt32(file.Length),
                    Url = "/Content/img/uploaded/" + templateimagemodel.ImagePath,
                    ThumbnailUrl = "/Content/img/uploaded/thumbnails/" + templateimagemodel.ImagePath,
                    DeleteUrl = String.Format("api/templates/{0}/images/{1}", id, templateimagemodel.TemplateImageID),
                    DeleteType = "DELETE",
                    Caption = templateimagemodel.Caption,
                    Default = templateimagemodel.Default
                });
            }

            return new TemplateImagesBindingModel
            {
                TemplateID = id,
                Files = files.ToArray()
            };
        }

        // GET api/templates/5/images/6
        [Route("{templateId:int}/images/{id:int}")]
        [Authorize(Roles = "Admin, Author")]
        public TemplateImagesBindingModel GetTemplateImageModel(int templateId, int id)
        {
            TemplateImageModel templateimagemodel = db.TemplateImageModels.Find(id);
            List<TemplateImageUploadedBindingModel> files = new List<TemplateImageUploadedBindingModel>();

            // Note: Most of this could be refactored into the UploadHandler.cs file but
            //       due to time constraints, we'll do it quick and dirty.
            //
            // TODO: Refactor
            FileInfo file = new FileInfo(String.Format(
                "{0}\\{1}",
                HttpContext.Current.Server.MapPath("/Content/img/uploaded"),
                templateimagemodel.ImagePath
            ));

            files.Add(new TemplateImageUploadedBindingModel
            {
                TemplateImageID = templateimagemodel.TemplateImageID,
                Name = templateimagemodel.Name,
                Size = Convert.ToInt32(file.Length),
                Url = "/Content/img/uploaded/" + templateimagemodel.ImagePath,
                ThumbnailUrl = "/Content/img/uploaded/thumbnails/" + templateimagemodel.ImagePath,
                DeleteUrl = String.Format("api/templates/{0}/images/{1}", id, templateimagemodel.TemplateImageID),
                DeleteType = "DELETE",
                Caption = templateimagemodel.Caption,
                Default = templateimagemodel.Default
            });

            return new TemplateImagesBindingModel
            {
                TemplateID = id,
                Files = files.ToArray()
            };
        }

        // PUT api/templates/5/images/6
        [Route("{templateId:int}/images/{id:int}")]
        [Authorize(Roles = "Admin, Author")]
        [ResponseType(typeof(TemplateImageDefaultBindingModel))]
        public async Task<IHttpActionResult> PutImageDefault(int templateId, int id, TemplateImageDefaultBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (templateId != model.TemplateID || id != model.TemplateImageID)
            {
                return BadRequest();
            }

            if (!TemplateModelBelongsToUserOrIsAdmin(templateId))
            {
                return BadRequest();
            }

            // Remove previous model
            TemplateImageModel oldmodel = await db.TemplateImageModels.FirstOrDefaultAsync(t => t.Default == true);

            if (oldmodel != null)
            {
                oldmodel.Default = false;

                db.TemplateImageModels.Attach(oldmodel);
                db.Entry(oldmodel).Property(c => c.Default).IsModified = true;
                await db.SaveChangesAsync();
            }

            // Update new model
            TemplateImageModel templateimagemodel = await db.TemplateImageModels.FindAsync(id);
            templateimagemodel.Default = model.Default;

            db.TemplateImageModels.Attach(templateimagemodel);
            db.Entry(templateimagemodel).Property(c => c.Default).IsModified = true;
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT api/templates/5/images
        [Route("{id:int}/images")]
        [Authorize(Roles = "Admin, Author")]
        [ResponseType(typeof(TemplateImageCaptionsBindingModel))]
        public async Task<IHttpActionResult> PutImageCaptions(int id, TemplateImageCaptionsBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.TemplateID)
            {
                return BadRequest();
            }

            if (!TemplateModelBelongsToUserOrIsAdmin(id))
            {
                return BadRequest();
            }

            // Iterate through the binding model and edit license entries
            foreach (TemplateImageCaptionBindingModel captionModel in model.Captions)
            {
                try
                {
                    TemplateImageModel templateimagemodel = await db.TemplateImageModels.FindAsync(captionModel.TemplateImageID);
                    templateimagemodel.Caption = captionModel.Caption;

                    db.TemplateImageModels.Attach(templateimagemodel);
                    db.Entry(templateimagemodel).Property(c => c.Caption).IsModified = true;
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT api/templates/5/showcase
        [Route("{id:int}/showcase", Name = "PostShowcase")]
        [Authorize(Roles = "Admin, Author")]
        [ResponseType(typeof(TemplateShowcaseBindingModel))]
        public async Task<IHttpActionResult> PutShowcase(int id, TemplateShowcaseBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.TemplateID)
            {
                return BadRequest();
            }

            if (!TemplateModelBelongsToUserOrIsAdmin(id))
            {
                return BadRequest();
            }

            // Update new model
            TemplateModel templatemodel = await db.TemplateModels.FindAsync(id);
            
            // OWASP 2
            // -------
            // Sanitize HTML input before inserting into database. Note this is the only database insertion
            // that outputs pure HTML back to the browser in the entire application. Every single other piece
            // of data being output from the database is displayed as plain text on the browser.
            //
            // The below script stored in Logic/HtmlUtility and uses HtmlAgilityPack http://htmlagilitypack.codeplex.com/
            // will strip out any tag not in my whitelist and sanitize the input before storing it in the database.
            templatemodel.HTMLShowcase  = HtmlUtility.SanitizeHtml(model.HTMLShowcase);

            db.TemplateModels.Attach(templatemodel);
            db.Entry(templatemodel).Property(t => t.HTMLShowcase).IsModified = true;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TemplateModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/templates/5/images
        [Route("{id:int}/images", Name = "PostImages")]
        [Authorize(Roles = "Admin, Author")]
        [ResponseType(typeof(TemplateImagesBindingModel))]
        public async Task<IHttpActionResult> PostImages(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!TemplateModelBelongsToUserOrIsAdmin(id))
            {
                return BadRequest();
            }

            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                return InternalServerError();
            }

            UploadHandler uploadFile = new UploadHandler(HttpContext.Current.Request.Files[0]);

            // If the file fails to save, return 500 server error
            if (!uploadFile.SaveFile())
            {
                return InternalServerError();
            }

            // Create TemplateImageModel
            TemplateImageModel templateimagemodel = new TemplateImageModel
            {
                Name       = uploadFile.FileName,
                ImagePath  = uploadFile.FileHash,
                Active     = true,
                TemplateID = id,
                Default    = false
            };

            db.TemplateImageModels.Add(templateimagemodel);

            await db.SaveChangesAsync();

            // Create response model
            TemplateImagesBindingModel model = new TemplateImagesBindingModel
            {
                TemplateID = id,
                Files = new TemplateImageUploadedBindingModel[]
                { 
                    new TemplateImageUploadedBindingModel
                    {
                        Name         = uploadFile.FileName,
                        Size         = uploadFile.FileSize,
                        Url          = uploadFile.FileUrl,
                        ThumbnailUrl = uploadFile.ThumbnailUrl,
                        DeleteUrl    = uploadFile.GetDeleteUrl(id, templateimagemodel.TemplateImageID),
                        DeleteType   = uploadFile.DeleteType
                    }
                }
            };

            return CreatedAtRoute("PostImages", new { id = model.TemplateID }, model);
        }

        // DELETE api/templates/5/images/6
        [Route("{templateId:int}/images/{id:int}")]
        [Authorize(Roles = "Admin, Author")]
        [ResponseType(typeof(TemplateImageModel))]
        public async Task<IHttpActionResult> DeleteTemplateImageModel(int templateId, int id)
        {
            TemplateImageModel model = await db.TemplateImageModels.FindAsync(id);
            if (model == null)
            {
                return NotFound();
            }

            if (!TemplateModelBelongsToUserOrIsAdmin(templateId))
            {
                return BadRequest();
            }

            db.TemplateImageModels.Remove(model);
            await db.SaveChangesAsync();

            // Delete files from file system
            FileInfo file = new FileInfo(String.Format(
                "{0}\\{1}",
                HttpContext.Current.Server.MapPath("/Content/img/uploaded"),
                model.ImagePath
            ));

            FileInfo thumbnail = new FileInfo(String.Format(
                "{0}\\{1}",
                HttpContext.Current.Server.MapPath("/Content/img/uploaded/thumbnails"),
                model.ImagePath
            ));

            try
            {
                file.Delete();
                thumbnail.Delete();
            }
            catch (Exception)
            {
                return InternalServerError();
            }

            return Ok(model);
        }

        /************************************************************
         * Other
         *************************************************************/

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TemplateModelExists(int id)
        {
            return db.TemplateModels.Count(e => e.TemplateID == id) > 0;
        }

        private bool TemplateModelBelongsToUserOrIsAdmin(int id)
        {
            var userId = User.Identity.GetUserId();
            IList<string> userRoles = UserManager.GetRoles(userId);

            return userRoles[0] == "Admin" ? true : (db.TemplateModels.Find(id).AuthorID == userId);
        }

    }
}