﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TemplatesDojo.Models;

namespace TemplatesDojo.Controllers
{
    [RoutePrefix("api/licenses")]
    public class LicensesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/Licenses
        public IQueryable<LicenseModel> GetLicenseModels()
        {
            return db.LicenseModels;
        }

        // GET api/Licenses/5
        [Route("{id:int}")]
        [ResponseType(typeof(LicenseModel))]
        public async Task<IHttpActionResult> GetLicenseModel(int id)
        {
            LicenseModel licensemodel = await db.LicenseModels.FindAsync(id);
            if (licensemodel == null)
            {
                return NotFound();
            }

            return Ok(licensemodel);
        }

        // PUT api/Licenses/5
        public async Task<IHttpActionResult> PutLicenseModel(int id, LicenseModel licensemodel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != licensemodel.LicenseID)
            {
                return BadRequest();
            }

            db.Entry(licensemodel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LicenseModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Licenses
        [ResponseType(typeof(LicenseModel))]
        public async Task<IHttpActionResult> PostLicenseModel(LicenseModel licensemodel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LicenseModels.Add(licensemodel);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = licensemodel.LicenseID }, licensemodel);
        }

        // DELETE api/Licenses/5
        [ResponseType(typeof(LicenseModel))]
        public async Task<IHttpActionResult> DeleteLicenseModel(int id)
        {
            LicenseModel licensemodel = await db.LicenseModels.FindAsync(id);
            if (licensemodel == null)
            {
                return NotFound();
            }

            db.LicenseModels.Remove(licensemodel);
            await db.SaveChangesAsync();

            return Ok(licensemodel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LicenseModelExists(int id)
        {
            return db.LicenseModels.Count(e => e.LicenseID == id) > 0;
        }
    }
}