﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TemplatesDojo.Models
{
    [Table("TemplateCategory")]
    public class TemplateCategoryModel
    {
        [Key]
        [Column(Order = 0)]
        public int TemplateID { get; set; }

        [Key]
        [Column(Order = 1)]
        public int CategoryID { get; set; }

        [ForeignKey("TemplateID")]
        public virtual TemplateModel Template { get; set; }

        [ForeignKey("CategoryID")]
        public virtual CategoryModel Category { get; set; }
    }
}