﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TemplatesDojo.Models
{
    public class OrderBindingModel
    {
        [Required]
        public OrderUserBindingModel User { get; set; }

        [Required]
        public OrderPaymentBindingModel Payment { get; set; }

        [Required]
        public OrderTemplateBindingModel[] Templates { get; set; }
    }

    public class OrderTemplateBindingModel
    {
        [Required]
        public int TemplateID { get; set; }

        [Required]
        public int LicenseID { get; set; }

        [Required]
        public double Price { get; set; }
    }

    public class OrderUserBindingModel
    {
        [Required]
        [StringLength(40)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(40)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [StringLength(40)]
        public string Telephone { get; set; }

        [Required]
        [StringLength(500)]
        public string Address { get; set; }
    }

    public class OrderPaymentBindingModel
    {
        [Required]
        public bool UseExisting { get; set; }

        [Required]
        public string CardNumber { get; set; }

        [Required]
        public DateTime Expiry { get; set; }

        [Required]
        [StringLength(90)]
        public string NameOnCard { get; set; }
    }

}