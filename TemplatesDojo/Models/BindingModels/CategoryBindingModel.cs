﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TemplatesDojo.Models
{
    public class CategoryActiveBindingModel
    {
        [Required]
        public int CategoryID { get; set; }

        [Required]
        public bool Active { get; set; }
    }

    public class CategoryHierarchyBindingModel
    {
        public CategoryModel ParentCategory { get; set; }

        public CategoryModel[] Subcategories { get; set; }
    }

}