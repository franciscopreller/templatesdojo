﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TemplatesDojo.Models
{
    public class TemplateBindingModel
    {
        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        [Required]
        [StringLength(400)]
        public string ShortDescription { get; set; }

        [StringLength(4000)]
        public string HTMLShowcase { get; set; }

        [StringLength(200)]
        public string Tags { get; set; }

        [Required]
        public int[] CategoryIDs { get; set; }

        [Required]
        public TemplateLicensePriceBindingModel[] Pricing { get; set; }
    }

    public class TemplateCategoriesBindingModel
    {
        [Required]
        public int TemplateID { get; set; }

        [Required]
        public int[] CategoryIDs { get; set; }
    }

    public class TemplatePricingBindingModel
    {
        [Required]
        public int TemplateID { get; set; }

        [Required]
        public TemplateLicensePriceBindingModel[] PricingModels { get; set; }
    }

    public class TemplateLicensePriceBindingModel
    {
        [Required]
        public int LicenseID { get; set; }

        [Required]
        public double Price { get; set; }
    }

    public class TemplateActiveBindingModel
    {
        [Required]
        public int TemplateID { get; set; }

        [Required]
        public bool Active { get; set; }
    }

    public class TemplateImagesBindingModel
    {
        [Required]
        public int TemplateID { get; set; }

        [Required]
        public TemplateImageUploadedBindingModel[] Files { get; set; }
    }

    public class TemplateImageUploadedBindingModel
    {
        public int TemplateImageID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Size { get; set; }

        [Required]
        public string Url { get; set; }

        [Required]
        public string ThumbnailUrl { get; set; }

        [Required]
        public string DeleteUrl { get; set; }

        [Required]
        public string DeleteType { get; set; }

        public string Caption { get; set; }

        public bool Default { get; set; }
    }

    public class TemplateImageCaptionsBindingModel
    {
        [Required]
        public int TemplateID { get; set; }

        [Required]
        public TemplateImageCaptionBindingModel[] Captions { get; set; }
    }

    public class TemplateImageCaptionBindingModel
    {
        [Required]
        public int TemplateImageID { get; set; }

        public string Caption { get; set; }
    }

    public class TemplateImageDefaultBindingModel
    {
        [Required]
        public int TemplateID { get; set; }

        [Required]
        public int TemplateImageID { get; set; }

        [Required]
        public bool Default { get; set; }
    }

    public class TemplateShowcaseBindingModel
    {
        [Required]
        public int TemplateID { get; set; }

        public string HTMLShowcase { get; set; }
    }

}