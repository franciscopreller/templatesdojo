﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TemplatesDojo.Models
{
    [Table("Payment")]
    public class PaymentModel
    {
        [Key]
        public int PaymentID { get; set; }

        [Required]
        public string UserID { get; set; }

        [Required]
        [StringLength(64)]
        public string CreditCardHash { get; set; }

        [Required]
        public int CreditCardTrail { get; set; }

        [Required]
        [StringLength(90)]
        public string NameOnCard { get; set; }

        [Required]
        public DateTime ExpiryDate { get; set; }

        [Required]
        public bool Active { get; set; }

        [ForeignKey("UserID")]
        public virtual ApplicationUser User { get; set; }
    }
}