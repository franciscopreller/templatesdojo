﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TemplatesDojo.Models
{
    [Table("Order")]
    public class OrderModel
    {
        [Key]
        public int OrderID { get; set; }

        [Required]
        public int PaymentID { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public string UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual ApplicationUser User { get; set; }

        [ForeignKey("PaymentID")]
        public virtual PaymentModel Payment { get; set; }

        public virtual ICollection<OrderTemplateModel> Templates { get; set; }
    }
}