﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TemplatesDojo.Models
{
    [Table("OrderTemplate")]
    public class OrderTemplateModel
    {
        [Key]
        [Column(Order = 0)]
        public int OrderID { get; set; }

        [Key]
        [Column(Order = 1)]
        public int TemplateID { get; set; }

        [Required]
        public int LicenseID { get; set; }

        // Prices may change in future, so best to store 
        // how much user paid for it TODAY...
        [Required]
        public double Price { get; set; } 

        [ForeignKey("OrderID")]
        public virtual OrderModel Order { get; set; }

        [ForeignKey("TemplateID")]
        public virtual TemplateModel Template { get; set; }

        [ForeignKey("LicenseID")]
        public virtual LicenseModel License { get; set; }
    }
}