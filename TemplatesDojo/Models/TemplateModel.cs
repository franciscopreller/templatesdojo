﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TemplatesDojo.Models
{
    [Table("Template")]
    public class TemplateModel
    {
        public TemplateModel()
        {
            Downloads = 0; // set default
            Active = true;
        }

        [Key]
        public int TemplateID { get; set; }

        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        [StringLength(4000)]
        public string HTMLShowcase { get; set; }

        [Required]
        [StringLength(400)]
        public string ShortDescription { get; set; }

        public int Downloads { get; set; }

        public DateTime CreationDate { get; set; }

        [StringLength(200)]
        public string Tags { get; set; }

        [Required]
        public bool Active { get; set; }

        public string AuthorID { get; set; }

        [ForeignKey("AuthorID")]
        public virtual ApplicationUser Author { get; set; }

        public virtual ICollection<TemplatePricingModel> Pricing { get; set; }

        public virtual ICollection<TemplateImageModel> Images { get; set; }

        public virtual ICollection<TemplateCategoryModel> Categories { get; set; }
    }
}