﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace TemplatesDojo.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("DefaultConnection")
        {
        }

        public DbSet<CategoryModel> CategoryModels { get; set; }
        public DbSet<TemplateModel> TemplateModels { get; set; }
        public DbSet<TemplateCategoryModel> TemplateCategoryModels { get; set; }
        public DbSet<TemplatePricingModel> TemplatePricingModels { get; set; }
        public DbSet<TemplateImageModel> TemplateImageModels { get; set; }
        public DbSet<LicenseModel> LicenseModels { get; set; }
        public DbSet<OrderModel> OrderModels { get; set; }
        public DbSet<OrderTemplateModel> OrderTemplateModels { get; set; }
        public DbSet<PaymentModel> PaymentModels { get; set; }
    }
}