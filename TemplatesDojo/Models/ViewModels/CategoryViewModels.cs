﻿using System;
using System.Collections.Generic;

namespace TemplatesDojo.Models
{
    public class CategoryManagementViewModel
    {
        public int CategoryID { get; set; }

        public int ParentCategoryID { get; set; }

        public string ParentName { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public bool Active { get; set; }
    }
}