﻿using System;
using System.Collections.Generic;

namespace TemplatesDojo.Models
{
    // Models returned by AccountController actions.

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string UserName { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }
    }

    public class UserInfoViewModel
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Telephone { get; set; }

        public DateTime CreationDate { get; set; }

        public int Access { get; set; }

        public PaymentModel Payment { get; set; }
    }

    public class UserViewModel
    {
        public string UserID { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Telephone { get; set; }

        public DateTime CreationDate { get; set; }

        public string Role { get; set; }

        public bool Active { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
}
