﻿using System;
using System.Collections.Generic;

namespace TemplatesDojo.Models
{
    public class TemplateManagementViewModel
    {
        public int TemplateID { get; set; }

        public string Name { get; set; }

        public DateTime CreationDate { get; set; }

        public int Downloads { get; set; }

        public bool Active { get; set; }

        public string AuthorID { get; set; }

        public string AuthorName { get; set; }
    }
}