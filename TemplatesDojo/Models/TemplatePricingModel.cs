﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TemplatesDojo.Models
{
    [Table("TemplatePricing")]
    public class TemplatePricingModel
    {
        [Key]
        [Column(Order = 0)]
        public int TemplateID { get; set; }

        [Key]
        [Column(Order = 1)]
        public int LicenseID { get; set; }

        [Required]
        public double Price { get; set; }

        [ForeignKey("TemplateID")]
        public TemplateModel Template { get; set; }

        [ForeignKey("LicenseID")]
        public LicenseModel License { get; set; }
    }
}