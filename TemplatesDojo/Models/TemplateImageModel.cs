﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TemplatesDojo.Models
{
    [Table("TemplateImage")]
    public class TemplateImageModel
    {
        public TemplateImageModel()
        {
            Active = true; // set default
        }

        [Key]
        public int TemplateImageID { get; set; }

        [StringLength(40)]
        public string Name { get; set; }

        [StringLength(90)]
        public string ImagePath { get; set; }

        [StringLength(500)]
        public string Caption { get; set; }

        public bool Active { get; set; }

        public bool Default { get; set; }

        public int TemplateID { get; set; }

        [ForeignKey("TemplateID")]
        public TemplateModel Template { get; set; }
    }
}