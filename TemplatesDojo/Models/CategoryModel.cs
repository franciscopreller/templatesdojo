﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TemplatesDojo.Models
{
    [Table("Category")]
    public class CategoryModel
    {
        public CategoryModel()
        {
            Active = true;
        }

        [Key]
        public int CategoryID { get; set; }

        [Required]
        [StringLength(20)]
        public string Code { get; set; }

        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        [StringLength(2000)]
        public string Description { get; set; }

        [Required]
        public bool Active { get; set; }

        public int? ParentCategoryID { get; set; }

        [ForeignKey("ParentCategoryID")]
        public virtual CategoryModel Category { get; set; }
    }

}