﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;

namespace TemplatesDojo.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [StringLength(40)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(40)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [StringLength(500)]
        public string Address { get; set; }

        [Required]
        [StringLength(40)]
        public string Telephone { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public bool Active { get; set; }
    }
}