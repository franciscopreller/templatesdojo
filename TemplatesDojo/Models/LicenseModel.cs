﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TemplatesDojo.Models
{
    [Table("License")]
    public class LicenseModel
    {
        [Key]
        public int LicenseID { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [Required]
        [StringLength(2000)]
        public string Terms { get; set; }
    }
}