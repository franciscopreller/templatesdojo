﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using TemplatesDojo.Models;

namespace TemplatesDojo.Providers
{
    // OWASP 3
    // -------
    // I am using oAuth authentication via Asp.Net Identity for Entity Framework 6. This produces a bearer token which
    // is then returned to the client-side. The token is then passed back to the back-end via a custom HTTP header
    // and checked on every single call to the server.

    // OWASP 5
    // -------
    // oAuth 2.0 also provides a layer of protection against CSRF attacks and therefore removes the need to use anti-csrf
    // tokens. The bearer token is generated at every login and acts as both a validation of user credentials and to
    // let the server know that the user is who they say they are. These tokens are not transmitted via the session but
    // instead over HTTP headers.
    
    // OWASP 7
    // -------
    // Asp.Net Identity takes care of the password hashing and salting here.

    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private readonly Func<UserManager<ApplicationUser>> _userManagerFactory;

        public ApplicationOAuthProvider(string publicClientId, Func<UserManager<ApplicationUser>> userManagerFactory)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            if (userManagerFactory == null)
            {
                throw new ArgumentNullException("userManagerFactory");
            }

            _publicClientId     = publicClientId;
            _userManagerFactory = userManagerFactory;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            using (UserManager<ApplicationUser> userManager = _userManagerFactory())
            {
                ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

                if (user == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }

                // Custom code :: Get a list of roles for user
                IList<string> userRoles = await userManager.GetRolesAsync(user.Id);
                
                // We will only use one role per user, so retrieve the first role and 
                // compare it with the roles and return the access level numerically.
                string access = (

                    userRoles[0] == "Admin" ?  4 : 
                    userRoles[0] == "Author" ? 3 : 
                    userRoles[0] == "Member" ? 2 : 1

                ).ToString();

                // AspNet generated code
                ClaimsIdentity oAuthIdentity        = await userManager.CreateIdentityAsync(user, context.Options.AuthenticationType);
                AuthenticationProperties properties = CreateProperties(user.UserName, access);
                AuthenticationTicket ticket         = new AuthenticationTicket(oAuthIdentity, properties);
                context.Validated(ticket);

                // Remove cookie authentication - not needed, also to enforce anti-CSSRF protection (OWASP 5)
                // ClaimsIdentity cookiesIdentity = await userManager.CreateIdentityAsync(user, CookieAuthenticationDefaults.AuthenticationType);
                // context.Request.Context.Authentication.SignIn(cookiesIdentity);
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName, string access)
        {
            // These values are returned as a response to the login along with the bearer token
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "access",   access }
            };
            return new AuthenticationProperties(data);
        }
    }
}