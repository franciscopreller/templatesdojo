namespace TemplatesDojo.Migrations
{
    using System.Data.Entity.Migrations;
    using TemplatesDojo.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            SeedData.PopulateIdentityModel(context);
            SeedData.PopulateLicenseModel(context);
            SeedData.PopulateCategoryModel(context);
            SeedData.PopulateTemplateModel(context);
            SeedData.PopulateTemplateCategoryModel(context);
            SeedData.PopulateTemplatePricingModel(context);
            SeedData.PopulatePaymentModel(context);
            SeedData.PopulateOrderModel(context);
            SeedData.PopulateOrderTemplateModel(context);
        }
    }
}
