﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using TemplatesDojo.Models;

namespace TemplatesDojo.Migrations
{
    public partial class SeedData
    {
        public static void PopulateTemplateCategoryModel(ApplicationDbContext context)
        {
            context.TemplateCategoryModels.AddOrUpdate(TemplateCategoryModels);
        }

        private static TemplateCategoryModel[] TemplateCategoryModels = new TemplateCategoryModel[]
        {
            new TemplateCategoryModel() {
                TemplateID = 1,
                CategoryID = 6
            },
            new TemplateCategoryModel() {
                TemplateID = 1,
                CategoryID = 9
            },
            new TemplateCategoryModel() {
                TemplateID = 1,
                CategoryID = 17
            },
            new TemplateCategoryModel() {
                TemplateID = 1,
                CategoryID = 25
            },
            new TemplateCategoryModel() {
                TemplateID = 1,
                CategoryID = 12
            },
            new TemplateCategoryModel() {
                TemplateID = 2,
                CategoryID = 11
            },
            new TemplateCategoryModel() {
                TemplateID = 2,
                CategoryID = 26
            },
            new TemplateCategoryModel() {
                TemplateID = 3,
                CategoryID = 13
            },
            new TemplateCategoryModel() {
                TemplateID = 3,
                CategoryID = 22
            },
            new TemplateCategoryModel() {
                TemplateID = 3,
                CategoryID = 18
            }
        };
    }

}