﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using TemplatesDojo.Models;

namespace TemplatesDojo.Migrations
{
    public partial class SeedData
    {
        public static void PopulateTemplateModel(ApplicationDbContext context)
        {
            context.TemplateModels.AddOrUpdate(TemplateModels("author"));
        }

        private static TemplateModel[] TemplateModels(string userName)
        {
            return new TemplateModel[]
            {
                new TemplateModel() {
                    TemplateID = 1,
                    Name = "Sample Template",
                    HTMLShowcase = "<p><strong>Unveil the Best. Template. EVER...</strong></p><p>Sample Template is a great theme created by the creators of other hit themes such as ACME Inc Template and Hello World. It can be used for nearly all purposes: <strong>Internet marketing, product launch, opt-in pages, call to action, blog, kitchen sinks and toaster dish-washers.</strong></p><p>Once you find yourself submitting your soul to this template, there is no going back to vanilla HTML land.</p>",
                    ShortDescription = "Awesome Template full of Lorem Ipsum and other goodies. It has everything you would ever need to be an extremely successful whatever you want. You must buy this now before someone else snatches it for good!",
                    Downloads = 35,
                    CreationDate = new DateTime(2013, 9, 30),
                    AuthorID = GetIdentityUserId(userName),
                    Tags = "portfolio corporate technology retail personal blog magazine buddypress wordpress joomla drupal umbraco dotnetnuke magento zen-cart shopify opencart oscommerce facebook email landing"
                },
                new TemplateModel() {
                    TemplateID = 2,
                    Name = "Photography",
                    HTMLShowcase = "",
                    ShortDescription = "Beautiful, captivating and expressive. This dark theme will draw viewers into further exploration.",
                    Downloads = 5,
                    CreationDate = new DateTime(2013, 9, 30),
                    AuthorID = GetIdentityUserId(userName),
                    Tags = "creative photography portfolio personal joomla"
                },
                new TemplateModel() {
                    TemplateID = 3,
                    Name = "Online Dating",
                    HTMLShowcase = "",
                    ShortDescription = "Play cupid and invite lots of sexy singles to join your new site and start meeting others.",
                    Downloads = 1,
                    CreationDate = new DateTime(2013, 9, 30),
                    AuthorID = GetIdentityUserId(userName),
                    Tags = "dating singles meet others bright sexy"
                }
            };
        }
    }

}