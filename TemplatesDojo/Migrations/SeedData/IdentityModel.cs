﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using TemplatesDojo.Models;

namespace TemplatesDojo.Migrations
{
    public partial class SeedData
    {
        private static Dictionary<string, string> Users = new Dictionary<string, string>();

        public static string GetIdentityUserId(string userName)
        {
            return Users[userName];
        }

        public static void PopulateIdentityModel(ApplicationDbContext context)
        {

            // Add user & roles
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // Roles
            RoleManager.Create(new IdentityRole("Admin"));
            RoleManager.Create(new IdentityRole("Author"));
            RoleManager.Create(new IdentityRole("Member"));

            // Member user
            ApplicationUser member = new ApplicationUser
            {
                UserName = "member",
                FirstName = "User",
                LastName = "Ninja",
                Email = "member@templatesdojo.com",
                Address = "9 Dojo Lane\nSydney, Australia",
                CreationDate = DateTime.Now,
                Telephone = "0430 557 557",
                Active = true
            };

            // Author user
            ApplicationUser author = new ApplicationUser
            {
                UserName = "author",
                FirstName = "Author",
                LastName = "Ninja Warrior",
                Email = "author@templatesdojo.com",
                Address = "5 Bamboo Way\nMelbourne, Australia",
                CreationDate = DateTime.Now,
                Telephone = "1300 556 556",
                Active = true
            };

            // Admin user
            ApplicationUser admin = new ApplicationUser
            {
                UserName = "admin",
                FirstName = "Administrator",
                LastName = "Ninja Master",
                Email = "admin@templatesdojo.com",
                Address = "7 Zen Boulevard\nBrisbane, Australia",
                CreationDate = DateTime.Now,
                Telephone = "1300 555 555",
                Active = true
            };

            if (UserManager.Create(member, "member123").Succeeded)
                UserManager.AddToRole(member.Id, "Member");

            if (UserManager.Create(author, "author123").Succeeded)
                UserManager.AddToRole(author.Id, "Author");

            if (UserManager.Create(admin, "admin123").Succeeded)
                UserManager.AddToRole(admin.Id, "Admin");

            Users.Add(member.UserName, member.Id);
            Users.Add(author.UserName, author.Id);
            Users.Add(admin.UserName, admin.Id);
        }
    }

}