﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using TemplatesDojo.Models;

namespace TemplatesDojo.Migrations
{
    public partial class SeedData
    {
        public static void PopulateCategoryModel(ApplicationDbContext context)
        {
            context.CategoryModels.AddOrUpdate(CategoryModels);
        }

        private static CategoryModel[] CategoryModels = new CategoryModel[]
        {
            // Main categories
            new CategoryModel() {
                CategoryID = 1,
                Code = "website",
                Name = "Website",
                Description = "Templates for simple yet awesome HTML5 websites."
            },
            new CategoryModel() {
                CategoryID = 2,
                Code = "wordpress",
                Name = "Wordpress",
                Description = "Beautiful themes to bring your Wordpress site to life."
            },
            new CategoryModel() {
                CategoryID = 3,
                Code = "cms",
                Name = "CMS",
                Description = "Themes and Templates for all the popular Content Management Systems."
            },
            new CategoryModel() {
                CategoryID = 4,
                Code = "e-commerce",
                Name = "eCommerce",
                Description = "Get highly wanted attention for the site when it matters the most. Beautiful sites sell more stuff!"
            },
            new CategoryModel() {
                CategoryID = 5,
                Code = "marketing",
                Name = "Marketing",
                Description = "Need to get word out about your website? These templates can help!",
            },

            // Subcategories
            new CategoryModel() {
                CategoryID = 6,
                Code = "portfolio",
                Name = "Creative Portfolio",
                ParentCategoryID = 1
            },
            new CategoryModel() {
                CategoryID = 7,
                Code = "corporate",
                Name = "Corporate",
                ParentCategoryID = 1
            },
            new CategoryModel() {
                CategoryID = 8,
                Code = "technology",
                Name = "Technology",
                ParentCategoryID = 1
            },
            new CategoryModel() {
                CategoryID = 9,
                Code = "retail",
                Name = "Retail",
                ParentCategoryID = 1
            },
            new CategoryModel() {
                CategoryID = 10,
                Code = "personal",
                Name = "Personal",
                ParentCategoryID = 1
            },
            new CategoryModel() {
                CategoryID = 11,
                Code = "health",
                Name = "Health",
                ParentCategoryID = 1
            },
            new CategoryModel() {
                CategoryID = 12,
                Code = "blog",
                Name = "Blog",
                ParentCategoryID = 2
            },
            new CategoryModel() {
                CategoryID = 13,
                Code = "magazine",
                Name = "Magazine",
                ParentCategoryID = 2
            },
            new CategoryModel() {
                CategoryID = 14,
                Code = "buddypress",
                Name = "BuddyPress",
                ParentCategoryID = 2
            },
            new CategoryModel() {
                CategoryID = 15,
                Code = "joomla",
                Name = "Joomla",
                ParentCategoryID = 3
            },
            new CategoryModel() {
                CategoryID = 16,
                Code = "drupal",
                Name = "Drupal",
                ParentCategoryID = 3
            },
            new CategoryModel() {
                CategoryID = 17,
                Code = "umbraco",
                Name = "Umbraco",
                ParentCategoryID = 3
            },
            new CategoryModel() {
                CategoryID = 18,
                Code = "dotnetnuke",
                Name = "DotNetNuke",
                ParentCategoryID = 3
            },
            new CategoryModel() {
                CategoryID = 19,
                Code = "magento",
                Name = "Magento",
                ParentCategoryID = 4
            },
            new CategoryModel() {
                CategoryID = 20,
                Code = "zen-cart",
                Name = "Zen Cart",
                ParentCategoryID = 4
            },
            new CategoryModel() {
                CategoryID = 21,
                Code = "shopify",
                Name = "Shopify",
                ParentCategoryID = 4
            },
            new CategoryModel() {
                CategoryID = 22,
                Code = "open-cart",
                Name = "OpenCart",
                ParentCategoryID = 4
            },
            new CategoryModel() {
                CategoryID = 23,
                Code = "os-commerce",
                Name = "osCommerce",
                ParentCategoryID = 4
            },
            new CategoryModel() {
                CategoryID = 24,
                Code = "facebook",
                Name = "Facebook",
                ParentCategoryID = 5
            },
            new CategoryModel() {
                CategoryID = 25,
                Code = "email",
                Name = "HTML Email",
                ParentCategoryID = 5
            },
            new CategoryModel() {
                CategoryID = 26,
                Code = "landing",
                Name = "Landing Pages",
                ParentCategoryID = 5
            }
        };
    }

}