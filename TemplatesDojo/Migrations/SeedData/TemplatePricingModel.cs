﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using TemplatesDojo.Models;

namespace TemplatesDojo.Migrations
{
    public partial class SeedData
    {
        public static void PopulateTemplatePricingModel(ApplicationDbContext context)
        {
            context.TemplatePricingModels.AddOrUpdate(TemplatePricingModels);
        }

        private static TemplatePricingModel[] TemplatePricingModels = new TemplatePricingModel[]
        {
            new TemplatePricingModel() {
                TemplateID = 1,
                LicenseID = 1,
                Price = 10.00
            },
            new TemplatePricingModel() {
                TemplateID = 1,
                LicenseID = 2,
                Price = 500.00
            },
            new TemplatePricingModel() {
                TemplateID = 1,
                LicenseID = 3,
                Price = 3500.00
            },
            new TemplatePricingModel() {
                TemplateID = 2,
                LicenseID = 1,
                Price = 15.00
            },
            new TemplatePricingModel() {
                TemplateID = 2,
                LicenseID = 2,
                Price = 600.00
            },
            new TemplatePricingModel() {
                TemplateID = 2,
                LicenseID = 3,
                Price = 3800.00
            },
            new TemplatePricingModel() {
                TemplateID = 3,
                LicenseID = 1,
                Price = 25.00
            },
            new TemplatePricingModel() {
                TemplateID = 3,
                LicenseID = 2,
                Price = 750.00
            },
            new TemplatePricingModel() {
                TemplateID = 3,
                LicenseID = 3,
                Price = 4200.00
            }
        };
    }

}