﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using TemplatesDojo.Models;

namespace TemplatesDojo.Migrations
{
    public partial class SeedData
    {
        public static void PopulateLicenseModel(ApplicationDbContext context)
        {
            context.LicenseModels.AddOrUpdate(LicenseModels);
        }
        
        private static LicenseModel[] LicenseModels = new LicenseModel[]
        {
            new LicenseModel() {
                LicenseID = 1,
                Name = "Regular",
                Terms = "Use, by you or one client, in a single end product which end users are not charged for."
            },
            new LicenseModel() {
                LicenseID = 2,
                Name = "Extended",
                Terms = "Use, by you or one client, in a single end product which end users can be charged for."
            },
            new LicenseModel() {
                LicenseID = 3,
                Name = "Exclusive",
                Terms = "Use, by you or any number of clients, in multiple products which can be charged for. The template will also be removed from stock for your exclusive use."
            }
        };
    }

}