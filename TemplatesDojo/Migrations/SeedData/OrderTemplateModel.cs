﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using TemplatesDojo.Models;

namespace TemplatesDojo.Migrations
{
    public partial class SeedData
    {
        public static void PopulateOrderTemplateModel(ApplicationDbContext context)
        {
            context.OrderTemplateModels.AddOrUpdate(OrderTemplateModels);
        }

        private static OrderTemplateModel[] OrderTemplateModels = new OrderTemplateModel[]
        {
            new OrderTemplateModel() {
                OrderID = 1,
                TemplateID = 1,
                LicenseID = 2,
                Price = 500.00
            },
            new OrderTemplateModel() {
                OrderID = 1,
                TemplateID = 2,
                LicenseID = 1,
                Price = 15.00
            },
            new OrderTemplateModel() {
                OrderID = 2,
                TemplateID = 3,
                LicenseID = 2,
                Price = 750.00
            }
        };
    }
}