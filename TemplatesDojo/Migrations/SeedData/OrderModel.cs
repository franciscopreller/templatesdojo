﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using TemplatesDojo.Models;

namespace TemplatesDojo.Migrations
{
    public partial class SeedData
    {
        public static void PopulateOrderModel(ApplicationDbContext context)
        {
            context.OrderModels.AddOrUpdate(OrderModels("member"));
        }

        private static OrderModel[] OrderModels(string userName)
        {
            return new OrderModel[]
            {
                new OrderModel() {
                    OrderID = 1,
                    OrderDate = DateTime.Now,
                    UserID = GetIdentityUserId(userName),
                    PaymentID = 1
                },
                new OrderModel() {
                    OrderID = 2,
                    OrderDate = DateTime.Now,
                    UserID = GetIdentityUserId(userName),
                    PaymentID = 1
                }
            };
        }
    }
}