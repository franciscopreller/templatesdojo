﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using TemplatesDojo.Models;
using TemplatesDojo.Logic;

namespace TemplatesDojo.Migrations
{
    public partial class SeedData
    {
        public static void PopulatePaymentModel(ApplicationDbContext context)
        {
            context.PaymentModels.AddOrUpdate(PaymentModels("member"));
        }

        private static PaymentModel[] PaymentModels(string userId) 
        {
            return new PaymentModel[]
            {
                new PaymentModel() {
                    PaymentID = 1,
                    CreditCardHash = Hash.Sha256("123467812345678"),
                    CreditCardTrail = 5678,
                    NameOnCard = "John B. Smith",
                    ExpiryDate = new DateTime(2015, 1, 1),
                    UserID = GetIdentityUserId(userId),
                    Active = true
                }
            };
        }
    }
}