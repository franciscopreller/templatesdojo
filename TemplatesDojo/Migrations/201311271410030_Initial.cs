namespace TemplatesDojo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 20),
                        Name = c.String(nullable: false, maxLength: 40),
                        Description = c.String(maxLength: 2000),
                        Active = c.Boolean(nullable: false),
                        ParentCategoryID = c.Int(),
                    })
                .PrimaryKey(t => t.CategoryID)
                .ForeignKey("dbo.Category", t => t.ParentCategoryID)
                .Index(t => t.ParentCategoryID);
            
            CreateTable(
                "dbo.License",
                c => new
                    {
                        LicenseID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        Terms = c.String(nullable: false, maxLength: 2000),
                    })
                .PrimaryKey(t => t.LicenseID);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        PaymentID = c.Int(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        UserID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderID)
                .ForeignKey("dbo.Payment", t => t.PaymentID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID, cascadeDelete: true)
                .Index(t => t.PaymentID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Payment",
                c => new
                    {
                        PaymentID = c.Int(nullable: false, identity: true),
                        UserID = c.String(nullable: false, maxLength: 128),
                        CreditCardHash = c.String(nullable: false, maxLength: 64),
                        CreditCardTrail = c.Int(nullable: false),
                        NameOnCard = c.String(nullable: false, maxLength: 90),
                        ExpiryDate = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PaymentID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID, cascadeDelete: false)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserName = c.String(),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        FirstName = c.String(maxLength: 40),
                        LastName = c.String(maxLength: 40),
                        Email = c.String(maxLength: 255),
                        Address = c.String(maxLength: 500),
                        Telephone = c.String(maxLength: 40),
                        CreationDate = c.DateTime(),
                        Active = c.Boolean(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.LoginProvider, t.ProviderKey })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderTemplate",
                c => new
                    {
                        OrderID = c.Int(nullable: false),
                        TemplateID = c.Int(nullable: false),
                        LicenseID = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => new { t.OrderID, t.TemplateID })
                .ForeignKey("dbo.License", t => t.LicenseID, cascadeDelete: true)
                .ForeignKey("dbo.Order", t => t.OrderID, cascadeDelete: true)
                .ForeignKey("dbo.Template", t => t.TemplateID, cascadeDelete: true)
                .Index(t => t.LicenseID)
                .Index(t => t.OrderID)
                .Index(t => t.TemplateID);
            
            CreateTable(
                "dbo.Template",
                c => new
                    {
                        TemplateID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 40),
                        HTMLShowcase = c.String(maxLength: 4000),
                        ShortDescription = c.String(nullable: false, maxLength: 400),
                        Downloads = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        Tags = c.String(maxLength: 200),
                        Active = c.Boolean(nullable: false),
                        AuthorID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.TemplateID)
                .ForeignKey("dbo.AspNetUsers", t => t.AuthorID)
                .Index(t => t.AuthorID);
            
            CreateTable(
                "dbo.TemplateCategory",
                c => new
                    {
                        TemplateID = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TemplateID, t.CategoryID })
                .ForeignKey("dbo.Category", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.Template", t => t.TemplateID, cascadeDelete: true)
                .Index(t => t.CategoryID)
                .Index(t => t.TemplateID);
            
            CreateTable(
                "dbo.TemplateImage",
                c => new
                    {
                        TemplateImageID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 40),
                        ImagePath = c.String(maxLength: 90),
                        Caption = c.String(maxLength: 500),
                        Active = c.Boolean(nullable: false),
                        Default = c.Boolean(nullable: false),
                        TemplateID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TemplateImageID)
                .ForeignKey("dbo.Template", t => t.TemplateID, cascadeDelete: true)
                .Index(t => t.TemplateID);
            
            CreateTable(
                "dbo.TemplatePricing",
                c => new
                    {
                        TemplateID = c.Int(nullable: false),
                        LicenseID = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => new { t.TemplateID, t.LicenseID })
                .ForeignKey("dbo.License", t => t.LicenseID, cascadeDelete: true)
                .ForeignKey("dbo.Template", t => t.TemplateID, cascadeDelete: true)
                .Index(t => t.LicenseID)
                .Index(t => t.TemplateID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderTemplate", "TemplateID", "dbo.Template");
            DropForeignKey("dbo.TemplatePricing", "TemplateID", "dbo.Template");
            DropForeignKey("dbo.TemplatePricing", "LicenseID", "dbo.License");
            DropForeignKey("dbo.TemplateImage", "TemplateID", "dbo.Template");
            DropForeignKey("dbo.TemplateCategory", "TemplateID", "dbo.Template");
            DropForeignKey("dbo.TemplateCategory", "CategoryID", "dbo.Category");
            DropForeignKey("dbo.Template", "AuthorID", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderTemplate", "OrderID", "dbo.Order");
            DropForeignKey("dbo.OrderTemplate", "LicenseID", "dbo.License");
            DropForeignKey("dbo.Order", "PaymentID", "dbo.Payment");
            DropForeignKey("dbo.Payment", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Category", "ParentCategoryID", "dbo.Category");
            DropIndex("dbo.Order", new[] { "UserID" });
            DropIndex("dbo.OrderTemplate", new[] { "TemplateID" });
            DropIndex("dbo.TemplatePricing", new[] { "TemplateID" });
            DropIndex("dbo.TemplatePricing", new[] { "LicenseID" });
            DropIndex("dbo.TemplateImage", new[] { "TemplateID" });
            DropIndex("dbo.TemplateCategory", new[] { "TemplateID" });
            DropIndex("dbo.TemplateCategory", new[] { "CategoryID" });
            DropIndex("dbo.Template", new[] { "AuthorID" });
            DropIndex("dbo.OrderTemplate", new[] { "OrderID" });
            DropIndex("dbo.OrderTemplate", new[] { "LicenseID" });
            DropIndex("dbo.Order", new[] { "PaymentID" });
            DropIndex("dbo.Payment", new[] { "UserID" });
            DropIndex("dbo.AspNetUserClaims", new[] { "User_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.Category", new[] { "ParentCategoryID" });
            DropTable("dbo.TemplatePricing");
            DropTable("dbo.TemplateImage");
            DropTable("dbo.TemplateCategory");
            DropTable("dbo.Template");
            DropTable("dbo.OrderTemplate");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Payment");
            DropTable("dbo.Order");
            DropTable("dbo.License");
            DropTable("dbo.Category");
        }
    }
}
