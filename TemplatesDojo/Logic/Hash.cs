﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace TemplatesDojo.Logic
{
    public class Hash
    {
        public static string Sha256(string text)
        {
            SHA256Managed hashstring = new SHA256Managed();

            byte[] bytes = Encoding.UTF8.GetBytes(text);
            byte[] hash  = hashstring.ComputeHash(bytes);

            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }

            return hashString;
        }
    }
}