﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using TemplatesDojo.Models;

namespace TemplatesDojo.Logic
{
    public class UploadHandler
    {
        public string FileName { get; private set; }
        public string FileType { get; private set; }
        public string FileHash { get; private set; }
        public string FileUrl { get; private set; }
        public string ThumbnailUrl { get; private set; }
        public string DeleteType { get; private set; }
        public int FileSize { get; private set; }
        public string Extension { get; private set; }
        private string FullName { get; set; }
        private string FullThumbnailName { get; set; }

        private HttpPostedFile _file { get; set; }
        private string _uploadPath    = HttpContext.Current.Server.MapPath("/Content/img/uploaded");
        private string _thumbnailPath = HttpContext.Current.Server.MapPath("/Content/img/uploaded/thumbnails");
        private string _deleteUrlBase = "api/templates/{0}/images/{1}";
        private string _acceptedFileTypes = @"^.+\.((jpg)|(gif)|(jpeg)|(png))$";

        public UploadHandler(int templateImageId)
        {

        }

        public UploadHandler(HttpPostedFile file)
        {
            Extension = Path.GetExtension(file.FileName);
            FileName     = file.FileName;
            FileHash     = String.Format("{0}{1}", GenerateRandomFileName(), Extension);
            FileUrl      = String.Format("/Content/img/uploaded/{0}", FileHash);
            ThumbnailUrl = String.Format("/Content/img/uploaded/thumbnails/{0}", FileHash);
            FileType     = file.ContentType;
            FileSize     = file.ContentLength;
            DeleteType   = "DELETE";
            FullName     = _uploadPath + "\\" + FileHash;
            FullThumbnailName = _thumbnailPath + "\\" + FileHash;
            _file = file;
        }

        public string GetDeleteUrl(int templateId, int imageId)
        {
            return String.Format(_deleteUrlBase, templateId, imageId);
        }

        public bool SaveFile()
        {
            // Make sure the upload directory exist
            DirectoryInfo dir = new DirectoryInfo(_uploadPath);
            if (!dir.Exists)
            {
                dir.Create();
            }

            // Validate the file type
            if (!Regex.IsMatch(FileName, _acceptedFileTypes, RegexOptions.Multiline | RegexOptions.IgnoreCase))
            {
                return false;
            }

            try
            {
                _file.SaveAs(_uploadPath + "\\" + FileHash);
                CreateThumbnail();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private bool CreateThumbnail()
        {
            // Make sure the upload/thumbnail directory exist
            DirectoryInfo dir = new DirectoryInfo(_thumbnailPath);
            if (!dir.Exists)
            {
                dir.Create();
            }

            Image image = Image.FromFile(FullName);
            ImageFormat imageType = GetImageType();

            if (image == null)
            {
                return false;
            }

            // Get width and height of image
            int width  = image.Width;
            int height = image.Height;

            if (width < 1 || height < 1)
            {
                return false;
            }

            // Let's force the thumbnail to have a height of 80 pixels or near
            float scale   = 80 / (float)height;
            int newHeight = (int)Math.Round(height * scale);
            int newWidth  = (int)Math.Round(width * scale);

            try
            {

                // Create the new image
                Bitmap newImage = new Bitmap(newWidth, newHeight);
                Graphics gfx = Graphics.FromImage(newImage);
                gfx.SmoothingMode = SmoothingMode.HighQuality;
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;

                foreach (PropertyItem prop in image.PropertyItems)
                {
                    newImage.SetPropertyItem(prop);
                }

                gfx.DrawImage(image, new Rectangle(0, 0, newWidth, newHeight));
                image.Dispose();

                newImage.Save(FullThumbnailName, imageType);
                newImage.Dispose();

            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private ImageFormat GetImageType()
        {
            return Extension == ".jpg" ? ImageFormat.Jpeg : Extension == ".gif" ? ImageFormat.Gif : ImageFormat.Png;
        }

        private string GenerateRandomFileName()
        {
            // Create a random filename
            var bytes = new byte[16];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(bytes);
            }

            return BitConverter.ToString(bytes).Replace("-", "");
        }
    }
}